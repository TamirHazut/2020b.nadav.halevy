package acs.rest;

import static org.assertj.core.api.Assertions.assertThat;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;
import acs.boundaries.ElementBoundary;
import acs.boundaries.ElementIdBoundary;
import acs.boundaries.UserBoundary;
import acs.data.UserRole;
import acs.logic.CreatedBy;
import acs.logic.Location;
import acs.logic.NewUserDetails;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class ElementControllerTest {
	private RestTemplate restTemplate;
	private String url;
	private String userURL;
	private int port;
	private String adminURL;
	private String adminUserURL;
	private String getElementsAsPlayerURL;
	private String domain, adminEmail, managerEmail, playerEmail;
	private UserBoundary manager;
	@LocalServerPort
	public void setPort(int port) {
		this.port = port;
	}

	@Value("${spring.application.name:acs}")
	public void setDomain(String domain) {
		this.domain = domain;
	}

	@PostConstruct
	public void init() {
		this.playerEmail = "playerEmail@test.com";
		this.managerEmail = "managerEmail@test.com";
		this.adminEmail = "adminEmail@test.com";

		final String url = "http://localhost:" + this.port;
		final String adminDetailsURL = "/" + this.domain + "/" + this.adminEmail;
		
		this.restTemplate = new RestTemplate();
		this.userURL = url + "/acs/users";
		this.adminURL = url + "/acs/admin/elements" + adminDetailsURL;
		this.adminUserURL = url + "/acs/admin/users" + adminDetailsURL;
		
		this.url = url + "/acs/elements/" + this.domain + "/" + this.managerEmail;
		this.getElementsAsPlayerURL = url + "/acs/elements/" + this.domain + "/" + this.playerEmail;
	}

	@AfterEach
	public void teardown() {
		this.restTemplate.delete(adminURL);
		this.restTemplate.delete(adminUserURL);
	}

	@BeforeEach
	public void setup() {
		this.restTemplate.postForObject(userURL, new NewUserDetails(this.adminEmail, UserRole.ADMIN, "admin", "admin"),
				UserBoundary.class);
		this.restTemplate.postForObject(userURL, new NewUserDetails(this.playerEmail, UserRole.PLAYER, "player", "player"),
				UserBoundary.class);
		this.manager = this.restTemplate.postForObject(userURL, new NewUserDetails(this.managerEmail, UserRole.MANAGER, "manager", "manager"),
				UserBoundary.class);
	}

	@Test
	void testElements() {

	}

	@Test
	void testCreateNewElementAndSaveItToTheDatabase() {
		// GIVEN the server is up
		// AND no elements
		this.restTemplate.delete(adminURL);

		// WHEN I POST a new Element
		ElementBoundary boundaryInput = new ElementBoundary(null, "type1", "name1", true, null,
				new CreatedBy(manager.getUserId()), new Location(1., 1.1), null);
		this.restTemplate.postForObject(this.url, boundaryInput, ElementBoundary.class);
		// Then the DB save the object
		assertThat(this.restTemplate.getForObject(this.url, ElementBoundary[].class)).hasSize(1);
	}

	@Test
	void testLocationUpdateOnElement() {
		// create ElementBoundary
		ElementBoundary boundaryInput = new ElementBoundary(null, "type1", "name1", true, null,
				new CreatedBy(manager.getUserId()), new Location(1., 1.1), null); 
		
		// insert into the DB and get it's ID
		boundaryInput = this.restTemplate.postForObject(this.url, boundaryInput, ElementBoundary.class);
		ElementIdBoundary inputId = boundaryInput.getElementId();
		
		Location locationUpdate = new Location(2., 2.08);

		// set the new location
		boundaryInput.setLocation(locationUpdate);
	
		// update the DB
		this.restTemplate.put(this.url + "/{elementDomain}/{elementId}", boundaryInput, inputId.getDomain(), inputId.getId()); 
		boundaryInput = this.restTemplate.getForObject(this.url + "/{elementDomain}/{elementId}"
				, ElementBoundary.class, inputId.getDomain(), inputId.getId());
		assertThat(boundaryInput.getLocation()).isEqualTo(locationUpdate);
	}

	@Test
	void testBindAnElementToAnOtherElementAsChild() {
		// Given the server is up
		// AND there are 2 elements
		ElementBoundary boundary1 = new ElementBoundary(null, "type1", "parent", true, null,
				new CreatedBy(manager.getUserId()), new Location(1., 1.1), null);
		ElementBoundary boundary2 = new ElementBoundary(null, "type2", "child", true, null,
				new CreatedBy(manager.getUserId()), new Location(2., 2.2), null);
		boundary1 = this.restTemplate.postForObject(this.url, boundary1, ElementBoundary.class);
		boundary2 = this.restTemplate.postForObject(this.url, boundary2, ElementBoundary.class);


		// WHEN I try to bind element1 to element2
		this.restTemplate.put(this.url + "/" + boundary1.getElementId().getDomain() + "/" + boundary1.getElementId().getId() + "/children",new ElementIdBoundary(boundary2.getElementId().getDomain(), boundary2.getElementId().getId()));

		// THEN element2 is in the children SET of element1
		ElementBoundary[] response = this.restTemplate.getForObject(
				this.getElementsAsPlayerURL + "/" + boundary1.getElementId().getDomain() + "/" + boundary1.getElementId().getId() + "/children", ElementBoundary[].class);
		assertThat(response).isNotNull();

	}
}
