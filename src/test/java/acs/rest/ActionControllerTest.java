package acs.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import acs.boundaries.ActionBoundary;
import acs.boundaries.ElementBoundary;
import acs.boundaries.ElementIdBoundary;
import acs.boundaries.UserBoundary;
import acs.data.UserRole;
import acs.logic.CreatedBy;
import acs.logic.Element;
import acs.logic.ElementId;
import acs.logic.InvokedBy;
import acs.logic.Location;
import acs.logic.NewUserDetails;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class ActionControllerTest {
	private RestTemplate restTemplate;
	private String userUrl;
	private String adminURL;
	private int port;
	private String actionURL;
	private String actionAdminURL;
	private String domain, adminEmail;
	private UserBoundary player;
	private UserBoundary manager;
	private String playerEmail;
	private String managerEmail;
	private String managerURL;

	@LocalServerPort
	public void setPort(int port) {
		this.port = port;
	}

	@Value("${spring.application.name:acs}")
	public void setDomain(String domain) {
		this.domain = domain;
	}

	@PostConstruct
	public void init() {
		this.playerEmail = "playerEmail@test.com";
		this.managerEmail = "managerEmail@test.com";
		this.adminEmail = "adminEmail@test.com";

		final String url = "http://localhost:" + this.port;
		final String adminDetailsURL = "/" + this.domain + "/" + this.adminEmail;
		final String managerDetailsURL = "/" + this.domain + "/" + this.managerEmail;

		this.restTemplate = new RestTemplate();
		this.actionURL = url + "/acs/actions";
		this.userUrl = url + "/acs/users";
		this.actionAdminURL = url + "/acs/admin/actions" + adminDetailsURL;
		this.adminURL = url + "/acs/admin/users" + adminDetailsURL;
		this.managerURL = url + "/acs/elements" + managerDetailsURL;
	}

	@BeforeEach
	public void setup() {
		this.restTemplate.postForObject(this.userUrl,
				new NewUserDetails(this.adminEmail, UserRole.ADMIN, "admin", "admin"), UserBoundary.class);
		this.restTemplate.delete(this.actionAdminURL);
	}

	@AfterEach
	public void teardown() {
		this.restTemplate.delete(adminURL);
	}

	@Test
	void testInvokeActionWthNullActionId() {
		// create player user
		this.player = this.restTemplate.postForObject(userUrl,
				new NewUserDetails(this.playerEmail, UserRole.PLAYER, "player", "player"), UserBoundary.class);

		// create manager user
		this.manager = this.restTemplate.postForObject(userUrl,
				new NewUserDetails(this.managerEmail, UserRole.MANAGER, "manager", "manager"), UserBoundary.class);
		// create restaurant
		ElementBoundary element = new ElementBoundary(null, "Element Type", "name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		element = this.restTemplate.postForObject(this.managerURL, element, ElementBoundary.class);
		// Adding new Action.
		ElementIdBoundary id = element.getElementId();

		this.restTemplate.postForObject(userUrl,
				new NewUserDetails(this.adminEmail, UserRole.ADMIN, "admin", "admin"), UserBoundary.class);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("address", "050");
		map.put("content", "content");
		this.restTemplate.postForObject(
				this.actionURL, new ActionBoundary(null, "New Order",
						new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map),
				Object.class);

		// Test action created
		ActionBoundary action = this.restTemplate.getForObject(this.actionAdminURL, ActionBoundary[].class)[0];
		// Test actionID created.
		assertThat(action).extracting("actionId").isNotEqualTo(null);
		// Test TimeStamp created.
		assertThat(action).extracting("createdTimestamp").isNotEqualTo(null);
	}

}
