package acs.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import acs.boundaries.ActionBoundary;
import acs.boundaries.ElementBoundary;
import acs.boundaries.ElementIdBoundary;
import acs.boundaries.UserBoundary;
import acs.data.UserRole;
import acs.logic.CreatedBy;
import acs.logic.Element;
import acs.logic.ElementId;
import acs.logic.InvokedBy;
import acs.logic.Location;
import acs.logic.NewUserDetails;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class orders {
	private RestTemplate restTemplate;
	private String url;
	private String userURL;
	private int port;
	private String adminURL;
	private String adminUserURL;
	private String domain, adminEmail, managerEmail, userEmail;
	private String managerURL;
	private UserBoundary player;
	private UserBoundary manager;
	private String actionsAdminURL;
	private String actionsURL;
	private String elementURL;

	@LocalServerPort
	public void setPort(int port) {
		this.port = port;
	}

	@Value("${spring.application.name:acs}")
	public void setDomain(String domain) {
		this.domain = domain;
	}

	@PostConstruct
	public void init() {
		this.userEmail = "userEmail@test.com";
		this.managerEmail = "managerEmail@test.com";
		this.adminEmail = "adminEmail@test.com";

		final String url = "http://localhost:" + this.port;
		final String adminDetailsURL = "/" + this.domain + "/" + this.adminEmail;
		final String managerDetailsURL = "/" + this.domain + "/" + this.managerEmail;

		this.restTemplate = new RestTemplate();
		this.userURL = url + "/acs/users";
		this.adminURL = url + "/acs/admin/elements" + adminDetailsURL;
		this.adminUserURL = url + "/acs/admin/users" + adminDetailsURL;
		this.actionsAdminURL = url + "/acs/admin/actions" + adminDetailsURL;
		this.url = url + "/acs/elements/" + this.domain + "/" + this.managerEmail;
		this.managerURL = url + "/acs/elements" + managerDetailsURL;
		this.actionsURL = url + "/acs/actions/";
		this.elementURL = url + "/acs/elements/";
	}

	@BeforeEach
	void setUp() throws Exception {
		this.restTemplate.postForObject(userURL, new NewUserDetails(this.adminEmail, UserRole.ADMIN, "admin", "admin"),
				UserBoundary.class);
	}

	@AfterEach
	void tearDown() throws Exception {
		this.restTemplate.delete(adminURL);
		this.restTemplate.delete(actionsAdminURL);
		this.restTemplate.delete(adminUserURL);
	}

	@Test
	void testOrder() {
	}

	@Test
	void test_Player_Place_Order_And_The_Restaurant_Got_1_New_Order() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// WHEN the player invoke 'New Order' Action
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);

		// THEN the restaurant got new order
		ElementBoundary[] result = this.restTemplate.getForObject(
				this.managerURL + "/{elementDomain}/{elementId}/children", ElementBoundary[].class, id.getDomain(),
				id.getId());
		assertThat(result).hasSize(1);
	}

	@Test
	void test_PLAYER_Invoke_New_Delivery_Order_Action_Then_Changes_Delivery_Person_Have_1_New_Delivery() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// AND there is at least 1 working delivery person with status WORKING_STATUS ==
		// true and ON_DELIVERY == false
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("WORKING_STATUS", "true");
		map.put("ON_DELIVERY", "false");
		ElementBoundary deliveryPerson = new ElementBoundary(null, "deliveryPerson", "Delivery Person Name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), map);
		deliveryPerson = this.restTemplate.postForObject(this.url, deliveryPerson, ElementBoundary.class);

		// AND there is at least 1 active order
		map.clear();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);
		ElementBoundary[] orders = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/search/byType/{type}", ElementBoundary[].class,
				manager.getUserId().getDomain(), manager.getUserId().getEmail(), "order");

		// WHEN the manager invoke 'New Delivery' Action after changes his role to
		// PLAYER
		map.put("deliveryPerson",
				deliveryPerson.getElementId().getDomain() + "," + deliveryPerson.getElementId().getId());
		this.manager.setRole(UserRole.PLAYER);
		this.restTemplate.put(userURL + "/{userDomain}/{userEmail}", this.manager, this.manager.getUserId().getDomain(),
				this.manager.getUserId().getEmail());
		ActionBoundary newDelivery = new ActionBoundary(null, "New Delivery Order",
				new Element(new ElementId(orders[0].getElementId().getDomain(), orders[0].getElementId().getId())), null,
				new InvokedBy(manager.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newDelivery, Object.class);

		// THEN the new delivery will be attached to the delivery person
		ElementBoundary[] result = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/{parentDomain}/{parentId}/children",
				ElementBoundary[].class, manager.getUserId().getDomain(), manager.getUserId().getEmail(),
				deliveryPerson.getElementId().getDomain(), deliveryPerson.getElementId().getId());
		assertThat(result).hasSize(1);
	}

	@Test
	void test_PLAYER_Invoke_New_Delivery_Order_Action_Then_The_Delivery_Will_Be_Attached_To_The_Delivery_Person() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// AND there is at least 1 working delivery person with status WORKING_STATUS ==
		// true and ON_DELIVERY == false
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("WORKING_STATUS", "true");
		map.put("ON_DELIVERY", "false");
		ElementBoundary deliveryPerson = new ElementBoundary(null, "deliveryPerson", "Delivery Person Name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), map);
		deliveryPerson = this.restTemplate.postForObject(this.url, deliveryPerson, ElementBoundary.class);

		// AND there is at least 1 active order
		map.clear();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);
		ElementBoundary[] orders = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/search/byType/{type}", ElementBoundary[].class,
				manager.getUserId().getDomain(), manager.getUserId().getEmail(), "order");

		// WHEN the manager invoke 'New Delivery' Action after changes his role to
		// PLAYER
		map.put("deliveryPerson",
				deliveryPerson.getElementId().getDomain() + "," + deliveryPerson.getElementId().getId());
		this.manager.setRole(UserRole.PLAYER);
		this.restTemplate.put(userURL + "/{userDomain}/{userEmail}", this.manager, this.manager.getUserId().getDomain(),
				this.manager.getUserId().getEmail());
		ActionBoundary newDelivery = new ActionBoundary(null, "New Delivery Order",
				new Element(new ElementId(orders[0].getElementId().getDomain(), orders[0].getElementId().getId())), null,
				new InvokedBy(manager.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newDelivery, Object.class);

		// THEN the new delivery will be attached to the delivery person
		ElementBoundary[] result = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/{parentDomain}/{parentId}/children",
				ElementBoundary[].class, manager.getUserId().getDomain(), manager.getUserId().getEmail(),
				deliveryPerson.getElementId().getDomain(), deliveryPerson.getElementId().getId());
		assertThat(result[0]).extracting("elementId.domain", "elementId.id")
				.containsExactly(orders[0].getElementId().getDomain(), orders[0].getElementId().getId());
	}

	@Test
	void test_PLAYER_Invoke_New_Delivery_Order_Action_Then_Changes_Delivery_Person_ON_DELIVERY_Attribute_Change_To_true() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// AND there is at least 1 working delivery person with status WORKING_STATUS ==
		// true and ON_DELIVERY == false
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("WORKING_STATUS", "true");
		map.put("ON_DELIVERY", "false");
		ElementBoundary deliveryPerson = new ElementBoundary(null, "deliveryPerson", "Delivery Person Name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), map);
		deliveryPerson = this.restTemplate.postForObject(this.url, deliveryPerson, ElementBoundary.class);

		// AND there is at least 1 active order
		map.clear();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);
		ElementBoundary[] orders = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/search/byType/{type}", ElementBoundary[].class,
				manager.getUserId().getDomain(), manager.getUserId().getEmail(), "order");

		// WHEN the manager invoke 'New Delivery' Action after changes his role to
		// PLAYER
		map.put("deliveryPerson",
				deliveryPerson.getElementId().getDomain() + "," + deliveryPerson.getElementId().getId());
		this.manager.setRole(UserRole.PLAYER);
		this.restTemplate.put(userURL + "/{userDomain}/{userEmail}", this.manager, this.manager.getUserId().getDomain(),
				this.manager.getUserId().getEmail());
		ActionBoundary newDelivery = new ActionBoundary(null, "New Delivery Order",
				new Element(new ElementId(orders[0].getElementId().getDomain(), orders[0].getElementId().getId())), null,
				new InvokedBy(manager.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newDelivery, Object.class);

		// THEN delivery person ON_DELIVERY attribute changes to true
		deliveryPerson = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/{elementDomain}/{elementId}", ElementBoundary.class,
				manager.getUserId().getDomain(), manager.getUserId().getEmail(),
				deliveryPerson.getElementId().getDomain(), deliveryPerson.getElementId().getId());
		assertThat(deliveryPerson.getElementAttributes().get("ON_DELIVERY")).isEqualTo("true");
	}

	@Test
	void test_PLAYER_Invoke_New_Delivery_Order_Action_Then_Changes_Order_Status_Change_To_DELIVERY_ON_THE_WAY_After_Create_New_Delivery() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// AND there is at least 1 working delivery person with status WORKING_STATUS ==
		// true and ON_DELIVERY == false
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("WORKING_STATUS", "true");
		map.put("ON_DELIVERY", "false");
		ElementBoundary deliveryPerson = new ElementBoundary(null, "deliveryPerson", "Delivery Person Name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), map);
		deliveryPerson = this.restTemplate.postForObject(this.url, deliveryPerson, ElementBoundary.class);

		// AND there is at least 1 active order
		map.clear();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);
		ElementBoundary[] orders = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/search/byType/{type}", ElementBoundary[].class,
				manager.getUserId().getDomain(), manager.getUserId().getEmail(), "order");

		// WHEN the manager invoke 'New Delivery' Action after changes his role to
		// PLAYER
		map.put("deliveryPerson",
				deliveryPerson.getElementId().getDomain() + "," + deliveryPerson.getElementId().getId());
		this.manager.setRole(UserRole.PLAYER);
		this.restTemplate.put(userURL + "/{userDomain}/{userEmail}", this.manager, this.manager.getUserId().getDomain(),
				this.manager.getUserId().getEmail());
		ActionBoundary newDelivery = new ActionBoundary(null, "New Delivery Order",
				new Element(new ElementId(orders[0].getElementId().getDomain(), orders[0].getElementId().getId())), null,
				new InvokedBy(manager.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newDelivery, Object.class);

		// THEN the order status changes to DELIVERY_ON_THE_WAY
		ElementBoundary[] result = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/{parentDomain}/{parentId}/children",
				ElementBoundary[].class, manager.getUserId().getDomain(), manager.getUserId().getEmail(),
				deliveryPerson.getElementId().getDomain(), deliveryPerson.getElementId().getId());
		assertThat(result[0].getElementAttributes().get("status")).isEqualTo("DELIVERY_ON_THE_WAY");
	}

	@Test
	void test_PLAYER_Invoke_Cancel_Order_Action_Then_Changes_Order_Status_To_Canceled() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// AND there is an order from that player
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);
		ElementBoundary[] orders = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/search/byType/{type}", ElementBoundary[].class,
				player.getUserId().getDomain(), player.getUserId().getEmail(), "order");

		ElementId orderId = new ElementId();
		for (ElementBoundary order : orders) {
			if (order.getElementAttributes().get("client")
					.equals(player.getUserId().getDomain() + "," + player.getUserId().getEmail())) {
				orderId.setDomain(order.getElementId().getDomain());
				orderId.setId(order.getElementId().getId());
			}
		}

		// WHEN the player invoke 'Cancel Delivery' Action
		ActionBoundary cancelDelivery = new ActionBoundary(null, "Cancel Order", new Element(orderId), null,
				new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, cancelDelivery, Object.class);

		// THEN the order status changes to CANCELED
		ElementBoundary order = this.restTemplate.getForObject(
				this.elementURL + "{userDomain}/{userEmail}/{elementDomain}/{elementId}", ElementBoundary.class,
				player.getUserId().getDomain(), player.getUserId().getEmail(), orderId.getDomain(), orderId.getId());
		assertThat(order.getElementAttributes().get("status")).isEqualTo("CANCELED");
	}

	@Test
	void test_PLAYER_Invoke_Cancel_Order_Action_Then_Changes_Active_To_false() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// AND there is an order from that player
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);
		ElementBoundary[] orders = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/search/byType/{type}", ElementBoundary[].class,
				player.getUserId().getDomain(), player.getUserId().getEmail(), "order");

		ElementId orderId = new ElementId();
		for (ElementBoundary order : orders) {
			if (order.getElementAttributes().get("client")
					.equals(player.getUserId().getDomain() + "," + player.getUserId().getEmail())) {
				orderId.setDomain(order.getElementId().getDomain());
				orderId.setId(order.getElementId().getId());
			}
		}

		// WHEN the player invoke 'Cancel Delivery' Action
		ActionBoundary cancelDelivery = new ActionBoundary(null, "Cancel Order", new Element(orderId), null,
				new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, cancelDelivery, Object.class);

		// THEN the order active attribute will be false
		ElementBoundary order = this.restTemplate.getForObject(
				this.elementURL + "{userDomain}/{userEmail}/{elementDomain}/{elementId}", ElementBoundary.class,
				player.getUserId().getDomain(), player.getUserId().getEmail(), orderId.getDomain(), orderId.getId());
		assertThat(order.getActive()).isEqualTo(false);
	}

	@Test
	void test_PLAYER_Invoke_Cancel_Order_Action_For_A_Delivery_Then_Changes_Delivery_Person_ON_DELIVERY_Attribute_To_false() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// AND there is at least 1 working delivery person with status WORKING_STATUS ==
		// true and ON_DELIVERY == false
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("WORKING_STATUS", "true");
		map.put("ON_DELIVERY", "false");
		ElementBoundary deliveryPerson = new ElementBoundary(null, "deliveryPerson", "Delivery Person Name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), map);
		deliveryPerson = this.restTemplate.postForObject(this.url, deliveryPerson, ElementBoundary.class);

		// AND there is an order from that player
		map = new HashMap<String, Object>();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);
		ElementBoundary[] orders = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/search/byType/{type}", ElementBoundary[].class,
				player.getUserId().getDomain(), player.getUserId().getEmail(), "order");

		ElementId orderId = new ElementId();
		for (ElementBoundary order : orders) {
			if (order.getElementAttributes().get("client")
					.equals(player.getUserId().getDomain() + "," + player.getUserId().getEmail())) {
				orderId.setDomain(order.getElementId().getDomain());
				orderId.setId(order.getElementId().getId());
			}
		}

		// AND the order is out for delivery
		map.put("deliveryPerson",
				deliveryPerson.getElementId().getDomain() + "," + deliveryPerson.getElementId().getId());
		ActionBoundary newDelivery = new ActionBoundary(null, "New Delivery Order",
				new Element(new ElementId(orders[0].getElementId().getDomain(), orders[0].getElementId().getId())), null,
				new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newDelivery, Object.class);

		// WHEN the player invoke 'Cancel Delivery' Action
		ActionBoundary cancelDelivery = new ActionBoundary(null, "Cancel Order", new Element(orderId), null,
				new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, cancelDelivery, Object.class);

		// THEN the delivery person ON_DELIVERY attribute will be false
		deliveryPerson = this.restTemplate.getForObject(
				this.elementURL + "{userDomain}/{userEmail}/{elementDomain}/{elementId}", ElementBoundary.class,
				player.getUserId().getDomain(), player.getUserId().getEmail(),
				deliveryPerson.getElementId().getDomain(), deliveryPerson.getElementId().getId());
		assertThat(deliveryPerson.getElementAttributes().get("ON_DELIVERY")).isEqualTo("false");
	}

	@Test
	void test_PLAYER_Invoke_Delivery_Received_Action_Then_Order_Changes_Order_Status_To_Recieved() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// AND there is at least 1 working delivery person with status WORKING_STATUS ==
		// true and ON_DELIVERY == false
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("WORKING_STATUS", "true");
		map.put("ON_DELIVERY", "false");
		ElementBoundary deliveryPerson = new ElementBoundary(null, "deliveryPerson", "Delivery Person Name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), map);
		deliveryPerson = this.restTemplate.postForObject(this.url, deliveryPerson, ElementBoundary.class);

		// AND there is an order from that player
		map = new HashMap<String, Object>();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);
		ElementBoundary[] orders = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/search/byType/{type}", ElementBoundary[].class,
				player.getUserId().getDomain(), player.getUserId().getEmail(), "order");

		ElementId orderId = new ElementId();
		for (ElementBoundary order : orders) {
			if (order.getElementAttributes().get("client")
					.equals(player.getUserId().getDomain() + "," + player.getUserId().getEmail())) {
				orderId.setDomain(order.getElementId().getDomain());
				orderId.setId(order.getElementId().getId());
			}
		}

		// AND the order is out for delivery
		map.put("deliveryPerson",
				deliveryPerson.getElementId().getDomain() + "," + deliveryPerson.getElementId().getId());
		ActionBoundary newDelivery = new ActionBoundary(null, "New Delivery Order",
				new Element(new ElementId(orders[0].getElementId().getDomain(), orders[0].getElementId().getId())), null,
				new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newDelivery, Object.class);

		// WHEN the player invoke 'Cancel Delivery' Action
		ActionBoundary deliveryReceived = new ActionBoundary(null, "Delivery Received", new Element(orderId), null,
				new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, deliveryReceived, Object.class);

		// THEN the order status changes to CANCELED
		ElementBoundary order = this.restTemplate.getForObject(
				this.elementURL + "{userDomain}/{userEmail}/{elementDomain}/{elementId}", ElementBoundary.class,
				player.getUserId().getDomain(), player.getUserId().getEmail(), orderId.getDomain(), orderId.getId());
		assertThat(order.getElementAttributes().get("status")).isEqualTo("RECEIVED");
	}

	@Test
	void test_PLAYER_Invoke_Delivery_Received_Action_Then_Order_Changes_Active_To_false() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// AND there is at least 1 working delivery person with status WORKING_STATUS ==
		// true and ON_DELIVERY == false
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("WORKING_STATUS", "true");
		map.put("ON_DELIVERY", "false");
		ElementBoundary deliveryPerson = new ElementBoundary(null, "deliveryPerson", "Delivery Person Name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), map);
		deliveryPerson = this.restTemplate.postForObject(this.url, deliveryPerson, ElementBoundary.class);

		// AND there is an order from that player
		map = new HashMap<String, Object>();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);
		ElementBoundary[] orders = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/search/byType/{type}", ElementBoundary[].class,
				player.getUserId().getDomain(), player.getUserId().getEmail(), "order");

		ElementId orderId = new ElementId();
		for (ElementBoundary order : orders) {
			if (order.getElementAttributes().get("client")
					.equals(player.getUserId().getDomain() + "," + player.getUserId().getEmail())) {
				orderId.setDomain(order.getElementId().getDomain());
				orderId.setId(order.getElementId().getId());
			}
		}

		// AND the order is out for delivery
		map.put("deliveryPerson",
				deliveryPerson.getElementId().getDomain() + "," + deliveryPerson.getElementId().getId());
		ActionBoundary newDelivery = new ActionBoundary(null, "New Delivery Order",
				new Element(new ElementId(orders[0].getElementId().getDomain(), orders[0].getElementId().getId())), null,
				new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newDelivery, Object.class);

		// WHEN the player invoke 'Cancel Delivery' Action
		ActionBoundary deliveryReceived = new ActionBoundary(null, "Delivery Received", new Element(orderId), null,
				new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, deliveryReceived, Object.class);

		// THEN the order active attribute will be false
		ElementBoundary order = this.restTemplate.getForObject(
				this.elementURL + "{userDomain}/{userEmail}/{elementDomain}/{elementId}", ElementBoundary.class,
				player.getUserId().getDomain(), player.getUserId().getEmail(), orderId.getDomain(), orderId.getId());
		assertThat(order.getActive()).isEqualTo(false);
	}

	@Test
	void test_PLAYER_Invoke_Delivery_Received_Action_Then_Changes_Delivery_Person_ON_DELIVERY_Attribute_To_false() {
		// GIVEN the server is up
		// AND there is at least 1 restaurant
		this.manager = createUser(managerEmail, UserRole.MANAGER, "manager");
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "restaurant name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		restaurant = this.restTemplate.postForObject(this.url, restaurant, ElementBoundary.class);

		// AND there is at least 1 player
		this.player = createUser(userEmail, UserRole.PLAYER, "player");
		ElementIdBoundary id = restaurant.getElementId();

		// AND there is at least 1 working delivery person with status WORKING_STATUS ==
		// true and ON_DELIVERY == false
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("WORKING_STATUS", "true");
		map.put("ON_DELIVERY", "false");
		ElementBoundary deliveryPerson = new ElementBoundary(null, "deliveryPerson", "Delivery Person Name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), map);
		deliveryPerson = this.restTemplate.postForObject(this.url, deliveryPerson, ElementBoundary.class);

		// AND there is an order from that player
		map = new HashMap<String, Object>();
		map.put("address", "050");
		map.put("content", "content");
		ActionBoundary newOrder = new ActionBoundary(null, "New Order",
				new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newOrder, Object.class);
		ElementBoundary[] orders = this.restTemplate.getForObject(
				this.elementURL + "/{userDomain}/{userEmail}/search/byType/{type}", ElementBoundary[].class,
				player.getUserId().getDomain(), player.getUserId().getEmail(), "order");

		ElementId orderId = new ElementId();
		for (ElementBoundary order : orders) {
			if (order.getElementAttributes().get("client")
					.equals(player.getUserId().getDomain() + "," + player.getUserId().getEmail())) {
				orderId.setDomain(order.getElementId().getDomain());
				orderId.setId(order.getElementId().getId());
			}
		}

		// AND the order is out for delivery
		map.put("deliveryPerson",
				deliveryPerson.getElementId().getDomain() + "," + deliveryPerson.getElementId().getId());
		ActionBoundary newDelivery = new ActionBoundary(null, "New Delivery Order",
				new Element(new ElementId(orders[0].getElementId().getDomain(), orders[0].getElementId().getId())), null,
				new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, newDelivery, Object.class);

		// WHEN the player invoke 'Delivery Received' Action
		ActionBoundary deliveryReceived = new ActionBoundary(null, "Delivery Received", new Element(orderId), null,
				new InvokedBy(player.getUserId()), map);
		this.restTemplate.postForObject(actionsURL, deliveryReceived, Object.class);

		// THEN the delivery person ON_DELIVERY attribute will be false
		deliveryPerson = this.restTemplate.getForObject(
				this.elementURL + "{userDomain}/{userEmail}/{elementDomain}/{elementId}", ElementBoundary.class,
				player.getUserId().getDomain(), player.getUserId().getEmail(),
				deliveryPerson.getElementId().getDomain(), deliveryPerson.getElementId().getId());
		assertThat(deliveryPerson.getElementAttributes().get("ON_DELIVERY")).isEqualTo("false");
	}

	private UserBoundary createUser(String email, UserRole role, String name) {
		return this.restTemplate.postForObject(userURL, new NewUserDetails(email, role, name, "avatar" + name),
				UserBoundary.class);
	}
}
