package acs.rest;

import static org.assertj.core.api.Assertions.assertThat;
import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import acs.boundaries.UserBoundary;
import acs.data.UserRole;
import acs.logic.NewUserDetails;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class UserControllerTest {
	private RestTemplate restTemplate;
	private String url;
	private String adminURL;
	private String domain, adminEmail;
	private int port;

	@LocalServerPort
	public void setPort(int port) {
		this.port = port;
	}

	@Value("${spring.application.name:acs}")
	public void setDomain(String domain) {
		this.domain = domain;
	}

	@PostConstruct
	public void init() {
		this.adminEmail = "adminEmail@test.com";
		final String url = "http://localhost:" + this.port;
		final String adminDetailsURL = "/" + this.domain + "/" + this.adminEmail; 
		
		this.restTemplate = new RestTemplate();
		this.url = url + "/acs/users";
		this.adminURL = url + "/acs/admin/users" + adminDetailsURL;
	}

	@AfterEach
	public void teardown() {
		this.restTemplate.delete(adminURL);
	}
	
	@BeforeEach
	public void setup() {
		this.restTemplate.postForObject(url,new NewUserDetails(this.adminEmail, UserRole.ADMIN, "admin", "admin"),UserBoundary.class);
	}

	@Test
	public void testUsers() {

	}

	@Test
	public void test_Post_New_User_Then_The_Response_Is_A_UserBoundary_With_The_Same_UserId() {
		// GIVEN the server is up

		// WHEN I POST a new User
		String email = "test1@afeka";
		UserBoundary userBoundary = this.restTemplate.postForObject(this.url,
				new NewUserDetails(email, UserRole.PLAYER, "Test1", ";)"), UserBoundary.class);

		// THEN the response is a UserBoundary with the same UserId
		assertThat(userBoundary.getUserId().getEmail()).isEqualTo(email);
	}

	@Test
	public void test_Get_User_From_Data_Base_With_A_Specific_UserId() {
		// GIVEN the server is up
		// AND the database is not Empty
		String domain = "2020b.nadav.halevy";
		String email = "test2@afeka";
		UserBoundary newUser = this.restTemplate.postForObject(this.url,
				new NewUserDetails(email, UserRole.ADMIN, "Test2", ";)"), UserBoundary.class);

		// WHEN I GET a user
		UserBoundary loginUser = this.restTemplate.getForObject(this.url + "/login/{userDomain}/{userEmail}",
				UserBoundary.class, domain, email);

		// THEN the UserId from the database is equal to the specific UserId
		assertThat(loginUser.getUserId()).isEqualTo(newUser.getUserId());
	}

	@Test
	public void test_Put_Of_A_UserBoundary_And_Updates_Is_Type_To_Manager_Then_Its_Updated_In_The_Database() {
		// GIVEN the server is up
		// AND the database is not Empty
		String domain = "2020b.nadav.halevy";
		String email = "test3@afeka";
		UserBoundary newUser = this.restTemplate.postForObject(this.url,
				new NewUserDetails(email, UserRole.MANAGER, "Test3", ";)"), UserBoundary.class);
		newUser.setRole(UserRole.MANAGER);

		// WHEN I PUT a user
		this.restTemplate.put(this.url + "/{userDomain}/{userEmail}", newUser, domain, email);

		// THEN The role changed to Manager in the database
		UserBoundary loginUser = this.restTemplate.getForObject(this.url + "/login/{userDomain}/{userEmail}",
				UserBoundary.class, domain, email);
		assertThat(loginUser).extracting("userId", "role").containsExactly(newUser.getUserId(), UserRole.MANAGER);
	}

}
