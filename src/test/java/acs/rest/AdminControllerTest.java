package acs.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import acs.boundaries.ActionBoundary;
import acs.boundaries.ElementBoundary;
import acs.boundaries.ElementIdBoundary;
import acs.boundaries.UserBoundary;
import acs.data.UserRole;
import acs.logic.CreatedBy;
import acs.logic.Element;
import acs.logic.ElementId;
import acs.logic.InvokedBy;
import acs.logic.Location;
import acs.logic.NewUserDetails;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class AdminControllerTest {
	private RestTemplate restTemplate;
	private String userAdminURL, elementsAdminURL, actionsAdminURL, userURL, actionURL, domain, adminEmail,
			managerEmail, playerEmail;
	private int port;
	private UserBoundary player;
	private UserBoundary manager;
	private String managerURL;

	@LocalServerPort
	public void setPort(int port) {
		this.port = port;
	}

	@Value("${spring.application.name:acs}")
	public void setDomain(String domain) {
		this.domain = domain;
	}

	@PostConstruct
	public void init() {
		this.playerEmail = "playerEmail@test.com";
		this.managerEmail = "managerEmail@test.com";
		this.adminEmail = "adminEmail@test.com";

		final String url = "http://localhost:" + this.port;
		final String adminDetailsURL = "/" + this.domain + "/" + this.adminEmail;
		final String managerDetailsURL = "/" + this.domain + "/" + this.managerEmail;

		this.restTemplate = new RestTemplate();
		this.userAdminURL = url + "/acs/admin/users" + adminDetailsURL;
		this.elementsAdminURL = url + "/acs/admin/elements" + adminDetailsURL;
		this.actionsAdminURL = url + "/acs/admin/actions" + adminDetailsURL;
		this.actionURL = url + "/acs/actions";
		this.userURL = url + "/acs/users";
		this.managerURL = url + "/acs/elements" + managerDetailsURL;

	}

	@BeforeEach
	void setUp() throws Exception {
		this.restTemplate.postForObject(userURL, new NewUserDetails(this.adminEmail, UserRole.ADMIN, "admin", "admin"),
				UserBoundary.class);
		this.restTemplate.delete(elementsAdminURL);
		this.restTemplate.delete(actionsAdminURL);

//		this.admin =this.restTemplate.postForObject(userURL, new NewUserDetails(this.adminEmail, UserRole.ADMIN, "admin", "admin"),
//				UserBoundary.class);
//		this.player = this.restTemplate.postForObject(userURL, new NewUserDetails(this.playerEmail, UserRole.PLAYER, "player", "player"),
//				UserBoundary.class);
//		this.manager = this.restTemplate.postForObject(userURL, new NewUserDetails(this.managerEmail, UserRole.MANAGER, "manager", "manager"),
//				UserBoundary.class);
	}

	@AfterEach
	void tearDown() throws Exception {
		this.restTemplate.delete(userAdminURL);
	}

	@Test
	void testAdmin() throws Exception {
	}

	@Test
	void testExportAllUsers() throws Exception {
		UserBoundary[] actual = this.restTemplate.getForObject(userAdminURL, UserBoundary[].class);
		assertThat(actual).hasSize(1);
	}

	@Test
	void testExportAllActions() throws Exception {

		// create player user
		this.player = this.restTemplate.postForObject(userURL,
				new NewUserDetails(this.playerEmail, UserRole.PLAYER, "player", "player"), UserBoundary.class);

		// create manager user
		this.manager = this.restTemplate.postForObject(userURL,
				new NewUserDetails(this.managerEmail, UserRole.MANAGER, "manager", "manager"), UserBoundary.class);
		// create restaurant
		ElementBoundary element = new ElementBoundary(null, "Element Type", "name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		element = this.restTemplate.postForObject(this.managerURL, element, ElementBoundary.class);
		// Adding new Action.
		ElementIdBoundary id = element.getElementId();

		this.restTemplate.postForObject(userURL,
				new NewUserDetails(this.adminEmail, UserRole.ADMIN, "admin", "admin"), UserBoundary.class);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("address", "050");
		map.put("content", "content");
		this.restTemplate.postForObject(
				this.actionURL, new ActionBoundary(null, "New Order",
						new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map),
				Object.class);

		// Testing Action was added.
		assertThat(this.restTemplate.getForObject(this.actionsAdminURL, ActionBoundary[].class)).hasSize(1);
	}

	@Test
	void testDeleteAllUsers() throws Exception {
		// adding 2nd player before delete for testing.
		createUser(this.playerEmail, UserRole.PLAYER, "player");
		createUser(adminEmail, UserRole.ADMIN, "admin");

		// Testing that we have 2 users before delete
		UserBoundary[] actual = this.restTemplate.getForObject(userAdminURL, UserBoundary[].class);
		assertThat(actual).hasSize(2);

		// Delete API send.
		this.restTemplate.delete(userAdminURL);

		// Testing that we have 1 user after delete (the user is added after the delete
		// for testing).
		this.restTemplate.postForObject(userURL, new NewUserDetails(this.adminEmail, UserRole.ADMIN, "admin", "admin"),
				UserBoundary.class);
		actual = this.restTemplate.getForObject(userAdminURL, UserBoundary[].class);
		assertThat(actual).hasSize(1);
	}

	@Test
	void testDeleteAllElements() throws Exception {
		this.manager = createUser(this.managerEmail, UserRole.MANAGER, "manager");

		// post a new Element
		ElementBoundary boundaryInput = new ElementBoundary(null, "type1", "name1", true, null,
				new CreatedBy(manager.getUserId()), new Location(1., 1.1), null);
		this.restTemplate.postForObject(this.managerURL, boundaryInput, ElementBoundary.class);

		// Testing Elements has created.
		assertThat(this.restTemplate.getForObject(this.managerURL, ElementBoundary[].class)).hasSize(1);

		// Delete API send.
		this.restTemplate.delete(this.elementsAdminURL);

		// Testing that the Element has been deleted.
		assertThat(this.restTemplate.getForObject(this.managerURL, ElementBoundary[].class)).hasSize(0);
	}

	@Test
	void testDeleteAllActions() throws Exception {
		// Adding new Action.
		// create player user
		this.player = this.restTemplate.postForObject(userURL,
				new NewUserDetails(this.playerEmail, UserRole.PLAYER, "player", "player"), UserBoundary.class);

		// create manager user
		this.manager = this.restTemplate.postForObject(userURL,
				new NewUserDetails(this.managerEmail, UserRole.MANAGER, "manager", "manager"), UserBoundary.class);
		// create restaurant
		ElementBoundary element = new ElementBoundary(null, "Element Type", "name", true, null,
				new CreatedBy(manager.getUserId()), new Location(1.1, 20.2), null);
		element = this.restTemplate.postForObject(this.managerURL, element, ElementBoundary.class);
		// Adding new Action.
		ElementIdBoundary id = element.getElementId();

		this.restTemplate.postForObject(userURL,
				new NewUserDetails(this.adminEmail, UserRole.ADMIN, "admin", "admin"), UserBoundary.class);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("address", "050");
		map.put("content", "content");
		this.restTemplate.postForObject(
				this.actionURL, new ActionBoundary(null, "New Order",
						new Element(new ElementId(id.getDomain(), id.getId())), null, new InvokedBy(player.getUserId()), map),
				Object.class);

		// Testing Action was added.
		assertThat(this.restTemplate.getForObject(this.actionsAdminURL, ActionBoundary[].class)).hasSize(1);

		// Delete API send.
		this.restTemplate.delete(this.actionsAdminURL);

		// Testing that the Action has been deleted.
		assertThat(this.restTemplate.getForObject(this.actionsAdminURL, ActionBoundary[].class)).hasSize(0);
	}

	private UserBoundary createUser(String email, UserRole role, String name) {
		return this.restTemplate.postForObject(userURL, new NewUserDetails(email, role, name, "avatar" + name),
				UserBoundary.class);
	}
}
