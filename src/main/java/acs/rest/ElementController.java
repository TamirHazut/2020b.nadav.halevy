package acs.rest;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import acs.boundaries.ElementBoundary;
import acs.boundaries.ElementIdBoundary;
import acs.logic.EnhancedElementService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class ElementController {
	EnhancedElementService elementService;

	@Autowired
	public ElementController(EnhancedElementService elementService) {
		this.elementService = elementService;
	}
	
	@RequestMapping(path = "/acs/admin/elements/{adminDomain}/{adminEmail}", method = RequestMethod.DELETE)
	public void deleteAllElements(@PathVariable("adminDomain") String adminDomain,
			@PathVariable("adminEmail") String adminEmail) {
		this.elementService.deleteAllElement(adminDomain, adminEmail);
	}

	@RequestMapping(path = "acs/elements/{managerDomain}/{managerEmail}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary createNewElement(@PathVariable("managerDomain") String managerDomain,
			@PathVariable("managerEmail") String managerEmail, @RequestBody ElementBoundary element) {
		ElementBoundary retval = this.elementService.create(managerDomain, managerEmail, element);
		return retval;
	}

	@RequestMapping(path = "/acs/elements/{managerDomain}/{managerEmail}/{elementDomain}/{elementId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateElement(@PathVariable("managerDomain") String managerDomain,
			@PathVariable("managerEmail") String managerEmail, @PathVariable("elementDomain") String elementDomain,
			@PathVariable("elementId") String elementId, @RequestBody ElementBoundary elementBoundary) {
		this.elementService.update(managerDomain, managerEmail, elementDomain, elementId, elementBoundary);
	}

	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}/{elementDomain}/{elementId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary getSpecificElement(@PathVariable("userDomain") String userDomain,
			@PathVariable("userEmail") String userEmail, @PathVariable("elementDomain") String elementDomain,
			@PathVariable("elementId") String elementId) {
		ElementBoundary retval = this.elementService.getSpecificElement(userDomain, userEmail, elementDomain,
				elementId);
		return retval;
	}

	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] getAllElements(@PathVariable("userDomain") String userDomain,
			@PathVariable("userEmail") String userEmail,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		ElementBoundary[] retval = this.elementService.getAllElements(userDomain, userEmail, size, page).toArray(new ElementBoundary[0]);
		return retval;
	}

	@RequestMapping(path = "/acs/elements/{managerDomain}/{managerEmail}/{elementDomain}/{elementId}/children", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void bindElementToChildElement(@RequestBody ElementIdBoundary child, @PathVariable String managerDomain,
			@PathVariable String managerEmail, @PathVariable String elementDomain, @PathVariable String elementId) {
		this.elementService.bindParentToChild(child, managerDomain, managerEmail, elementDomain, elementId);
	}

	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}/{elementDomain}/{elementId}/children", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] getAllElementChildren(@PathVariable String userDomain, @PathVariable String userEmail,
			@PathVariable String elementDomain, @PathVariable String elementId,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService.getAllElementChildren(userDomain, userEmail, elementDomain, elementId, size, page).toArray(new ElementBoundary[0]);
	}
	
	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}/{elementDomain}/{elementId}/parents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] getAllElementParents(@PathVariable String userDomain, @PathVariable String userEmail,
			@PathVariable String elementDomain, @PathVariable String elementId,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService.getAllElementParents(userDomain, userEmail, elementDomain, elementId, size, page).toArray(new ElementBoundary[0]);
	}
	
	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}/search/byName/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] getAllElementsByName(@PathVariable String userDomain, @PathVariable String userEmail,
			@PathVariable String name,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService.getAllElementsByName(userDomain, userEmail, name, size, page).toArray(new ElementBoundary[0]);
	}
	
	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}/search/byType/{type}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] getAllElementsByType(@PathVariable String userDomain, @PathVariable String userEmail,
			@PathVariable String type,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService.getAllElementsByType(userDomain, userEmail, type, size, page).toArray(new ElementBoundary[0]);
	}
	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}/search/near/{lat}/{lng}/{distance}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] getAllNearElements(@PathVariable String userDomain, @PathVariable String userEmail,
			@PathVariable double lat, @PathVariable double lng, @PathVariable double distance,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService.getAllNearElements(userDomain, userEmail, lat, lng, distance, size, page).toArray(new ElementBoundary[0]);
	}
	
	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	public Map<String, String> handleException(Exception e) {
		String error = e.getMessage();
		if (error == null) {
			error = "Something went wrong";
		}
		return Collections.singletonMap("error", error);
	}

}
