package acs.rest;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import acs.boundaries.UserBoundary;
import acs.logic.NewUserDetails;
import acs.logic.UserId;
import acs.logic.UserService;
import acs.logic.Exceptions.UnauthorizedUserException;
import acs.logic.Exceptions.UserNotFoundException;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class UserController {

	private UserService userService;

	@Autowired
	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}

	@RequestMapping(path = "/acs/admin/users/{adminDomain}/{adminEmail}", method = RequestMethod.DELETE)
	public void deleteAllUsers(@PathVariable("adminDomain") String adminDomain,
			@PathVariable("adminEmail") String adminEmail) {
		this.userService.deleteAllUsers(adminDomain, adminEmail);
	}
	
	@RequestMapping(path = "/acs/admin/users/{adminDomain}/{adminEmail}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserBoundary[] exportAllUsers(@PathVariable("adminDomain") String adminDomain,
			@PathVariable("adminEmail") String adminEmail,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.userService.getAllUsers(adminDomain, adminEmail, size, page).toArray(new UserBoundary[0]);
	}
	
	@RequestMapping(path = "/acs/users", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserBoundary createNewUserBoundary(@RequestBody NewUserDetails user) {
		UserBoundary userB = new UserBoundary();
		userB.setUserId(new UserId(user.getEmail()));
		userB.setUsername(user.getUsername());
		userB.setRole(user.getRole());
		userB.setAvatar(user.getAvatar());
		return userService.createUser(userB);
	}

	@RequestMapping(path = "/acs/users/{userDomain}/{userEmail}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateUserBoundary(@PathVariable("userDomain") String userDomain,
			@PathVariable("userEmail") String userEmail, @RequestBody UserBoundary update) {
		userService.updateUser(userDomain, userEmail, update);
	}

	@RequestMapping(path = "/acs/users/login/{userDomain}/{userEmail}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserBoundary login(@PathVariable("userDomain") String userDomain,
			@PathVariable("userEmail") String userEmail) {
		return userService.login(userDomain, userEmail);
	}

	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public Map<String, String> handleException(UserNotFoundException e) {
		String error = e.getMessage();
		if (error == null) {
			error = "Something went wrong";
		}
		return Collections.singletonMap("error", error);
	}
	
	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
	public Map<String, String> handleException(UnauthorizedUserException e) {
		String error = e.getMessage();
		if (error == null) {
			error = "Something went wrong";
		}
		return Collections.singletonMap("error", error);
	}

	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	public Map<String, String> handleException(RuntimeException e) {
		String error = e.getMessage();
		if (error == null) {
			error = "Something went wrong";
		}
		return Collections.singletonMap("error", error);
	}

	
}
