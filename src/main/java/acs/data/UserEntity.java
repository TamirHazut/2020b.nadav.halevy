package acs.data;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import acs.logic.UserId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Data
@Document(collection = "Users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6497461677440692265L;
	private @NonNull @Id UserId userId;
	private @NonNull UserRole role;
	private @NonNull String username;
	private @NonNull String avatar;
}
