package acs.data.converters;

import java.util.HashMap;

import org.springframework.stereotype.Component;

import acs.boundaries.ActionBoundary;
import acs.data.ActionEntity;
import acs.logic.Element;
import acs.logic.ElementId;
import acs.logic.InvokedBy;
import acs.logic.UserId;

@Component
public class ActionConverter {

	public ActionBoundary entityToBoundary(ActionEntity entity) {
		ActionBoundary boundary = new ActionBoundary(
				entity.getActionId(),
				entity.getType(),
				stringToElement(entity.getElement()),
				null,
				stringToInvokedBy(entity.getInvokedBy()),
				entity.getActionAttributes());
		boundary.setCreatedTimestamp(entity.getCreatedTimestamp());
		return boundary;
	}

	public ActionEntity boundaryToEntity(ActionBoundary boundary) {
		ActionEntity entity = new ActionEntity(
				boundary.getActionId(),
				boundary.getType(),
				elementToString(boundary.getElement()),
				boundary.getCreatedTimestamp(),
				invokedByToString(boundary.getInvokedBy()),
				(boundary.getActionAttributes() == null ? new HashMap<String, Object>()
						: boundary.getActionAttributes()));
		return entity;
	}
	
	public Element stringToElement(String elementAsString) {
		Element element = new Element(new ElementId());
		if (!elementAsString.isEmpty()) {
			String[] elementIdSplitted = elementAsString.split(",");
			element.getElementId().setDomain(elementIdSplitted[0]);
			element.getElementId().setId(elementIdSplitted[1]);
		}
		return element;
	}
	
	public String elementToString(Element element) {
		return element.getElementId().getDomain() + "," + element.getElementId().getId();
	}
	
	public InvokedBy stringToInvokedBy(String invokedByAsString) {
		InvokedBy invokedBy = new InvokedBy(new UserId());
		if (!invokedByAsString.isEmpty()) {
			String[] invokedBySplitted = invokedByAsString.split(",");
			invokedBy.getUserId().setDomain(invokedBySplitted[0]);
			invokedBy.getUserId().setEmail(invokedBySplitted[1]);
		}
		return invokedBy;
	}
	
	public String invokedByToString(InvokedBy invokedBy) {
		return invokedBy.getUserId().getDomain() + "," + invokedBy.getUserId().getEmail();
	}
}
