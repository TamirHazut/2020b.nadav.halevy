package acs.data.converters;

import org.springframework.stereotype.Component;

import acs.boundaries.UserBoundary;
import acs.data.UserEntity;
import acs.logic.UserId;

@Component
public class UserConverter {

	public UserBoundary entityToBoundary(UserEntity entity) {
		UserBoundary boundary = new UserBoundary();
		UserId userId = new UserId();
		if (entity.getUserId() != null) {
			userId = entity.getUserId();
		} 
		boundary.setUserId(userId);
		if (entity.getRole() != null) {
			boundary.setRole(entity.getRole());
		} else {
			boundary.setRole(null);
		}
		boundary.setUsername(entity.getUsername());
		boundary.setAvatar(entity.getAvatar());
		return boundary;
	}

	public UserEntity boundaryToEntity(UserBoundary boundary) {
		UserEntity entity = new UserEntity();
		if (boundary.getUserId() != null) {
			entity.setUserId(new UserId (boundary.getUserId().getDomain(), boundary.getUserId().getEmail()));
		} else {
			entity.setUserId(new UserId (boundary.getUserId().getDomain(), ""));
		}
		if (boundary.getRole() != null) {
			entity.setRole(boundary.getRole());
		} else {
			entity.setRole(null);
		}
		entity.setUsername(boundary.getUsername());
		entity.setAvatar(boundary.getAvatar());
		return entity;
	}
	
	public UserId stringsToUserId(String userDomain , String userEmail) {
		if (userEmail == null || userEmail.isEmpty())
			return null;
		return new UserId(userDomain,userEmail);
	}
	
	public UserId stringToUserId(String userIdAsString) {
		if (userIdAsString == null || userIdAsString.isEmpty())
			return null;
		String[] userId = userIdAsString.split(",");
		return new UserId(userId[0],userId[1]);
	}

	public String elementIdToString(UserId userId) {
		if(userId == null)
			return null;
		return userId.getDomain() + "," + userId.getEmail();
	}

}
