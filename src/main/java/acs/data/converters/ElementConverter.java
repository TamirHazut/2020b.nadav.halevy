package acs.data.converters;

import java.util.HashMap;

import org.springframework.data.geo.Point;
import org.springframework.stereotype.Component;
import acs.boundaries.ElementBoundary;
import acs.boundaries.ElementIdBoundary;
import acs.data.ElementEntity;
import acs.logic.CreatedBy;
import acs.logic.ElementId;
import acs.logic.Location;
import acs.logic.UserId;

@Component
public class ElementConverter {

	public ElementBoundary fromEntity(ElementEntity entity) {
		ElementBoundary boundary = new ElementBoundary(fromEntity(entity.getElementId()), entity.getType(),
				entity.getName(), entity.isActive(), entity.getCreatedTimestamp(), stringToCreatedBy(entity.getCreatedBy()),
				entity.getLocation(),
				entity.getElementAttributes());
		return boundary;
	}

	public ElementEntity toEntity(ElementBoundary element) {
		ElementEntity retval = new ElementEntity();
		retval.setElementId(toEntity(element.getElementId()));

		retval.setActive((element.getActive() == null) ? false : element.getActive());
		retval.setCreatedBy(createdByToString(element.getCreatedBy()));
		retval.setElementAttributes((element.getElementAttributes() == null ? new HashMap<String, Object>()
				: element.getElementAttributes()));
		retval.setLocation(element.getLocation());
		retval.setName(element.getName());
		retval.setType(element.getType());
		return retval;
	}

	public ElementId stringsToElementId(String elementDomain , String elementId) {
		if (elementId == null || elementId.isEmpty())
			return null;
		return new ElementId(elementDomain,elementId);
	}
	
	public ElementId stringToElementId(String elementIdAsString) {
		if (elementIdAsString == null || elementIdAsString.isEmpty())
			return null;
		String[] element = elementIdAsString.split(",");
		return new ElementId(element[0],element[1]);
	}

	public String elementIdToString(ElementId elementId) {
		if(elementId == null)
			return null;
		return elementId.getDomain() + "," + elementId.getId();
	}

	public CreatedBy stringToCreatedBy(String createdBy) {
		if (createdBy == null || createdBy.isEmpty())
			return null;
		String[] createdBySplited = createdBy.split(",");
		return new CreatedBy(new UserId(createdBySplited[0], createdBySplited[1]));

	}

	public String createdByToString(CreatedBy createdBy) {
		if (createdBy == null)
			return null;
		return createdBy.getUserId().getDomain() + "," + createdBy.getUserId().getEmail();
	}
	
	public Location pointToLocation(Point location) {
		if (location == null)
			return null;
		return new Location(location.getX(), location.getY());

	}
	
	public Point locationToPoint(Location location) {
		if (location == null)
			return null;
		return new Point(location.getLat(), location.getLng());
	}
	
	public ElementId toEntity(ElementIdBoundary bondaryId) {
		if (bondaryId == null || bondaryId.getDomain() == null || bondaryId.getId() == null) {
			return new ElementId();
		}
		return new ElementId(bondaryId.getDomain(), bondaryId.getId());
	}

	public ElementIdBoundary fromEntity(ElementId elementId) {
		if (elementId.getDomain() == null || elementId.getId() == null) {
			return null;
		}
		return new ElementIdBoundary(elementId.getDomain(), elementId.getId());
	}
}
