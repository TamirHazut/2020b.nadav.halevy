package acs.data.dal;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import acs.data.UserEntity;
import acs.data.UserRole;
import acs.logic.UserId;

public interface UserDataAccessRepository extends MongoRepository<UserEntity, UserId> {
	
	public List<UserEntity> findByRole(UserRole role, Pageable pageable);
	
}
