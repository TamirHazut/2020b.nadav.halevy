package acs.data.dal;

import org.springframework.data.mongodb.repository.MongoRepository;

import acs.data.ActionEntity;
import acs.logic.ActionId;

public interface ActionDataAccessRepository extends MongoRepository<ActionEntity, ActionId> {

}
