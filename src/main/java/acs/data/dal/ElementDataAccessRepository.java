package acs.data.dal;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import acs.data.ElementEntity;
import acs.logic.ElementId;

public interface ElementDataAccessRepository extends MongoRepository<ElementEntity, ElementId> {
	
	@Query(value = "{ 'parents': { $ref : 'Elements', $id: ?0 } }")
	public List<ElementEntity> findAllByParents(@Param("_id") ElementId parentId, Pageable pageable);
		
	@Query(value = "{ 'children': { $ref : 'Elements', $id: ?0 } }")
	public List<ElementEntity> findAllByChildren(@Param("_id") ElementId childId, Pageable pageable);
	
	public List<ElementEntity> findByType(String type, Pageable pageable);
	
	public List<ElementEntity> findByName(String name, Pageable pageable);
	
	@Query(value = "{ 'location': { $geoNear: [ ?0, ?1 ], $maxDistance: ?2 } }" )
	public List<ElementEntity> findAllNearElements(double lat, double lng, double distance, Pageable pageable);
	
	@Query( value = "{ 'elementAttributes.client': ?1, 'parents': { $ref : 'Elements', $id: ?0 } }")
	public List<ElementEntity> findAllPlayerActiveElements(@Param("_id") ElementId elementId, @Param("elementAttributes.client") String playerId);

}
