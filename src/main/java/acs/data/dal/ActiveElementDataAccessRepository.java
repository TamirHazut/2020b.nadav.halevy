package acs.data.dal;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import acs.data.ElementEntity;
import acs.logic.ElementId;

public interface ActiveElementDataAccessRepository extends ElementDataAccessRepository {

	@Query(value = "{ 'active': ?1, 'parents': { $ref : 'Elements', $id: ?0 } }")
	public List<ElementEntity> findAllByParentsAndActive(@Param("_id") ElementId parentId, @Param("active") boolean active, Pageable pageable);
	
	@Query(value = "{ 'active': ?1, 'children': { $ref : 'Elements', $id: ?0 } }")
	public List<ElementEntity> findAllByChildrenAndActive(@Param("_id") ElementId childId, @Param("active") boolean active, Pageable pageable);
	
	public List<ElementEntity> findByActive(boolean active, Pageable pageable);
	
	public List<ElementEntity> findByTypeAndActive(String type, boolean active, Pageable pageable);
	
	public List<ElementEntity> findByNameAndActive(String name, boolean active, Pageable pageable);
	
	@Query(value = "{ 'active': ?3, 'location': { $geoNear: [ ?0, ?1 ], $maxDistance: ?2 } }" )
	public List<ElementEntity> findAllNearActiveElements(double lat, double lng, double distance, boolean active, Pageable pageable);

	@Query( value = "{ 'active': ?2, 'elementAttributes.client': ?1, 'parents': { $ref : 'Elements', $id: ?0 } }")
	public List<ElementEntity> findAllPlayerActiveElements(@Param("_id") ElementId elementId, @Param("elementAttributes.client") String playerId, @Param("active") boolean active);
}