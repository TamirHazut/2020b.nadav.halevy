package acs.data;
import java.util.Date;
import java.util.Map;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import acs.logic.ActionId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Document(collection = "Actions")
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ActionEntity {

	private @NonNull @Id ActionId actionId;
	private @NonNull String type;
	private @NonNull String element;
	private @CreatedDate Date createdTimestamp;
	private @NonNull String invokedBy;
	private @NonNull Map<String, Object> actionAttributes;
}
