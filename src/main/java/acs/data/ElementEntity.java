package acs.data;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import acs.logic.ElementId;
import acs.logic.Location;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Data
@Document(collection = "Elements")
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ElementEntity implements Serializable {

//	/**
//	 * 
//	 */
	private static final long serialVersionUID = 1L;
	private @NonNull @Id ElementId elementId;
	private @NonNull String type;
	private @NonNull String name;
	private boolean active;
	private @NonNull @CreatedDate Date createdTimestamp;
	private @NonNull @CreatedBy String createdBy;
	private @NonNull Location location;
	private @NonNull Map<String, Object> elementAttributes;// = new HashMap<>();
	@EqualsAndHashCode.Exclude private @DBRef(lazy = true) @NonNull Set<ElementEntity> parents;
	@EqualsAndHashCode.Exclude private @DBRef(lazy = true) @NonNull Set<ElementEntity> children;

	
	public ElementEntity() {
		parents = new HashSet<>();
		elementAttributes = new HashMap<>();
		children = new HashSet<>();
	}
	
	public void bindParentToChild(ElementEntity child) {
		if (!this.getChildren().contains(child)) {
			this.getChildren().add(child);
		}
		if (!child.getParents().contains(this)) {
			child.getParents().add(this);
		}
	}
}
