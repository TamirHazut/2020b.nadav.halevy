package acs.boundaries;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ElementIdBoundary {
	/* 
	{
        "domain": "domain",
        "id": "id"
    }
	 */
	private @NonNull String domain;
	private @NonNull String id;
}
