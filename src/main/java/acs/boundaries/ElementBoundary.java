package acs.boundaries;

import java.util.Date;
import java.util.Map;

import acs.logic.CreatedBy;
import acs.logic.Location;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ElementBoundary {
	
	/*
{
    "elementId": {
        "domain": "domain",
        "id": "id"
    },
    "type": "type1",
    "name": "name1",
    "active": true,
    "createdTimestamp" : null,
    "createdBy": {
    	"userId" : {
        	"domain": "domain",
        	"email": "itay@gmail.com"
    	}
    },
    "location": {
        "lat": 3.1,
        "lng": 43.2
    }
}
	 */

	private ElementIdBoundary elementId;
	private @NonNull String type;
	private @NonNull String name;
	private @NonNull Boolean active;
	private Date createdTimestamp;
	private @NonNull CreatedBy createdBy;
	private @NonNull Location location;
	private Map<String, Object> elementAttributes;
}
