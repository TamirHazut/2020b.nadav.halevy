package acs.boundaries;

import java.util.Date;
import java.util.Map;

import acs.logic.ActionId;
import acs.logic.Element;
import acs.logic.InvokedBy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ActionBoundary {
/*
{
    "actionId": {
        "domain": "2020b.nadav.halevy",
        "id": "4c82dcb8-0d3d-45dc-8e97-fe12e0eb0ac4"
    },
    "type": "actionType",
    "element": {
        "elementId": {
            "domain": "2020b.nadav.halevy",
            "id": "60f37828-7379-4f63-9291-d599757bed51"
        }
    },
    "createdTimestamp": "2020-05-18T07:51:50.551+0000",
    "invokedBy": {
        "userId": {
            "domain": "2020b.nadav.halevy",
            "email": "player1@init.test"
        }
    },
    "actionAttributes": {
        "key1": "can be set to any value you wish",
        "key2": 44.5,
        "booleanValue": false,
        "lastKey": "it can contain anything you wish"
    }
}
 */
	private ActionId actionId;
	private @NonNull String type;
	private @NonNull Element element;
	private Date createdTimestamp;
	private @NonNull InvokedBy invokedBy;
	private @NonNull Map<String, Object> actionAttributes;
}
