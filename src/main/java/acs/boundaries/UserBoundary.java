package acs.boundaries;

import acs.data.UserRole;
import acs.logic.UserId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UserBoundary {
	/*
	 * {
	 * "userId" : {
        	"domain": "domain",
        	"email": "itay@gmail.com"
    	},
	 * "role"	:	"ADMIN",
	 * "username":	"username",
	 * "avatar"	:	"avater"
	 * }
	 */
	private @NonNull UserId userId;
	private @NonNull UserRole role;
	private @NonNull String username;
	private @NonNull String avatar;
}
