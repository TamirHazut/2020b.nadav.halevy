package acs.logic;

import acs.data.UserRole;

public interface CheckInputsService {

	public boolean isAdmin(String adminDomain, String adminEmail);
	
	public boolean isManager(String managerDomain, String managerEmail);
	
	public boolean isUser(String userDomain, String userEmail);
	
	public boolean isPlayer(String playerDomain, String playerEmail);

	public boolean isElementActive(ElementId elementId);
	
	public void email_Validation_check(String email);
	
	public void role_Validation_check(UserRole role) ;
	
	public  void username_Validation_check(String username);
	
	public  void element_Validation_check(String element);
	
	public  void avatar_Validation_check(String avatar);
	
	public  void type_Validation_check(String type);
	
	public  void invokedBy_Validation_check(String invokedBy);
}
