package acs.logic;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserId implements Comparable<UserId>, Serializable {

	/*
	 * {
	 * 	"domain" : "domain",
	 *	"email":"mail"
	 *	}
	 */
	private static final long serialVersionUID = 1L;
	private String domain;
	private @NonNull String email;
	
	@Override
	public int compareTo(UserId other) {
		if (other == null)
			return 1;
		
		if (this.domain.compareTo(other.domain) == 0)
			return this.email.compareTo(other.email);
		
		return this.domain.compareTo(other.domain);
	}

}
