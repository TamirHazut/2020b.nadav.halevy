package acs.logic;

import acs.data.UserRole;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class NewUserDetails {
	/*
	 * {
	 * "email"	:	"email",
	 * "role"	:	"ADMIN",
	 * "username":	"username",
	 * "avatar"	:	"avatar"
	 * }
	 */
	private @NonNull String email;
	private @NonNull UserRole role;
	private @NonNull String username;
	private @NonNull String avatar;
}
