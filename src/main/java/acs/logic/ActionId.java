package acs.logic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ActionId implements Comparable<ActionId> {
	
	/*
	 * {
	 * 		"domain":"domain",
	 * 		"id"	:"id"
	 * }
	 */
	private @NonNull String domain;
	private @NonNull String id;
	
	@Override
	public int compareTo(ActionId other) {
		if (other == null)
			return 1;
		
		if (this.domain.compareTo(other.domain) == 0)
			return this.id.compareTo(other.id);
		
		return this.domain.compareTo(other.domain);
	}

}
