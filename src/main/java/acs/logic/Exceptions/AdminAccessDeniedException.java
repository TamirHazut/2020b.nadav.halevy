package acs.logic.Exceptions;

public class AdminAccessDeniedException extends RuntimeException {

	private static final long serialVersionUID = 1252145138225103163L;

	public AdminAccessDeniedException(String message) {
		super(message);
	}

	public AdminAccessDeniedException(String message, Throwable cause) {
		super(message, cause);
	}

	public AdminAccessDeniedException(Throwable cause) {
		super(cause);
	}

}
