package acs.logic.Exceptions;

public class UserAlreadyExistsException extends RuntimeException {
	private static final long serialVersionUID = -8205169098590243459L;

	public UserAlreadyExistsException() {
	}

	public UserAlreadyExistsException(String message) {
		super(message);
	}

	public UserAlreadyExistsException(Throwable cause) {
		super(cause);
	}

	public UserAlreadyExistsException(String message, Throwable cause) {
		super(message, cause);
	}

}
