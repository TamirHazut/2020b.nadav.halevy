package acs.logic.Exceptions;

public class UnauthorizedUserException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2441415106921072389L;

	public UnauthorizedUserException(String message) {
		super(message + " is unauthorized");
		// TODO Auto-generated constructor stub
	}

	public UnauthorizedUserException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public UnauthorizedUserException(String message, Throwable cause) {
		super(message + " is unauthorized", cause);
		// TODO Auto-generated constructor stub
	}


}
