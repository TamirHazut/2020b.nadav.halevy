package acs.logic.Exceptions;

public class ManagerAccessDeniedException extends RuntimeException {
	private static final long serialVersionUID = 5954373923855309180L;

	public ManagerAccessDeniedException() {
		super();
	}

	public ManagerAccessDeniedException(String message) {
		super(message);
	}

	public ManagerAccessDeniedException(String message, Throwable cause) {
		super(message, cause);
	}

	public ManagerAccessDeniedException(Throwable cause) {
		super(cause);
	}

}
