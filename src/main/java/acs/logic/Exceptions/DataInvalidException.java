package acs.logic.Exceptions;

public class DataInvalidException extends RuntimeException {
	private static final long serialVersionUID = -8205169098590243459L;

	public DataInvalidException() {
	}

	public DataInvalidException(String message) {
		super(message);
	}

	public DataInvalidException(Throwable cause) {
		super(cause);
	}

	public DataInvalidException(String message, Throwable cause) {
		super(message, cause);
	}

}
