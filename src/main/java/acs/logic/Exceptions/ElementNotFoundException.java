package acs.logic.Exceptions;

public class ElementNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1324271117825486074L;

	public ElementNotFoundException() {
	}

	public ElementNotFoundException(String message) {
		super(message + " not found");
	}

	public ElementNotFoundException(Throwable cause) {
		super(cause);
	}

	public ElementNotFoundException(String message, Throwable cause) {
		super(message + " not found", cause);
	}

	public ElementNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message + " not found", cause, enableSuppression, writableStackTrace);
	}

}
