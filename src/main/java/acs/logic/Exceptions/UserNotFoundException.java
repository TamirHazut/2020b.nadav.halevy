package acs.logic.Exceptions;

public class UserNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -8205169098590243459L;

	public UserNotFoundException() {
	}

	public UserNotFoundException(String message) {
		super(message + " not found");
	}

	public UserNotFoundException(Throwable cause) {
		super(cause);
	}

	public UserNotFoundException(String message, Throwable cause) {
		super(message + " not found", cause);
	}

}
