package acs.logic.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import acs.data.ActionEntity;
import acs.data.ElementEntity;
import acs.data.converters.ActionConverter;
import acs.data.converters.ElementConverter;
import acs.data.dal.ElementDataAccessRepository;
import acs.logic.ElementId;
import acs.logic.Exceptions.ElementNotFoundException;

@Component
public class ActionCancelOrder implements ActionStrategy {
	private ActionConverter actionConverter;
	private ElementDataAccessRepository elementDataAccessRepository;
	private ElementConverter elementConverter;

	@Autowired
	public ActionCancelOrder(ActionConverter actionConverter, ElementDataAccessRepository elementDataAccessRepository, ElementConverter elementConverter) {
		super();
		this.actionConverter = actionConverter;
		this.elementDataAccessRepository = elementDataAccessRepository;
		this.elementConverter = elementConverter;
	}

	@Override
	public Object doAction(ActionEntity canceledDelivery) {
		ElementId orderId = this.actionConverter.stringToElement(canceledDelivery.getElement()).getElementId();
		ElementEntity order = this.elementDataAccessRepository.findById(orderId)
				.orElseThrow(() -> new ElementNotFoundException(orderId.getId()));

		if (order.getElementAttributes().get("status").equals("DELIVERY_ON_THE_WAY")) {
			ElementId deliveryPersonId = this.elementConverter
					.stringToElementId(order.getElementAttributes().get("deliveryPerson").toString());
			ElementEntity deliveryPerson = this.elementDataAccessRepository.findById(deliveryPersonId)
					.orElseThrow(() -> new ElementNotFoundException(deliveryPersonId.getId()));

			deliveryPerson.getElementAttributes().replace("ON_DELIVERY", "false");
			this.elementDataAccessRepository.save(deliveryPerson);
		}

		order.setActive(false);
		order.getElementAttributes().replace("status", "CANCELED");

		
		return this.elementConverter.fromEntity(this.elementDataAccessRepository.save(order));
	}

	@Override
	public String getActionType() {
		return "Cancel Order";
	}

}
