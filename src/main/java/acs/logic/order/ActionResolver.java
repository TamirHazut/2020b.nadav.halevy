package acs.logic.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import acs.data.ActionEntity;
import acs.data.converters.ElementConverter;
import acs.logic.Exceptions.DataInvalidException;

@Service
public class ActionResolver {
	private Map<String, ActionStrategy> actionTypesToActionsMap = new HashMap<>();

	@Autowired
	public ActionResolver(List<ActionStrategy> typesToActionsMap, ElementConverter elementConverter) {
		super();
		this.actionTypesToActionsMap = typesToActionsMap.stream()
				.collect(Collectors.toMap(ActionStrategy::getActionType, action -> action));
	}

	public Object doAction(ActionEntity action) {
		if (!actionTypesToActionsMap.containsKey(action.getType())) {
			throw new DataInvalidException("Unknown action");
		}
		return actionTypesToActionsMap.get(action.getType()).doAction(action);
	}
}
