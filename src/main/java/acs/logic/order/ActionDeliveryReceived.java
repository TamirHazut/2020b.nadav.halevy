package acs.logic.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import acs.data.ActionEntity;
import acs.data.ElementEntity;
import acs.data.converters.ActionConverter;
import acs.data.converters.ElementConverter;
import acs.data.dal.ElementDataAccessRepository;
import acs.logic.ElementId;
import acs.logic.Exceptions.ElementNotFoundException;

@Component
public class ActionDeliveryReceived implements ActionStrategy {
	private ActionConverter actionConverter;
	private ElementDataAccessRepository elementDataAccessRepository;
	private ElementConverter elementConverter;

	@Autowired
	public ActionDeliveryReceived(ActionConverter actionConverter, ElementDataAccessRepository elementDataAccessRepository, ElementConverter elementConverter) {
		super();
		this.actionConverter = actionConverter;
		this.elementDataAccessRepository = elementDataAccessRepository;
		this.elementConverter = elementConverter;
	}

	@Override
	public Object doAction(ActionEntity completedDelivery) {
		ElementId deliveryId = this.actionConverter.stringToElement(completedDelivery.getElement()).getElementId();
		ElementEntity delivery = this.elementDataAccessRepository.findById(deliveryId)
				.orElseThrow(() -> new ElementNotFoundException(deliveryId.getId()));

		ElementId deliveryPersonId = this.elementConverter
				.stringToElementId(delivery.getElementAttributes().get("deliveryPerson").toString());
		ElementEntity deliveryPerson = this.elementDataAccessRepository.findById(deliveryPersonId)
				.orElseThrow(() -> new ElementNotFoundException(deliveryPersonId.getId()));

		delivery.setActive(false);
		delivery.getElementAttributes().replace("status", "RECEIVED");

		deliveryPerson.getElementAttributes().replace("ON_DELIVERY", "false");
		this.elementDataAccessRepository.save(deliveryPerson);
		return this.elementConverter.fromEntity(this.elementDataAccessRepository.save(delivery));
	}

	@Override
	public String getActionType() {
		return "Delivery Received";
	}

}
