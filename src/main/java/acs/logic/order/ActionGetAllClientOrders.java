package acs.logic.order;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import acs.boundaries.ElementBoundary;
import acs.data.ActionEntity;
import acs.data.converters.ActionConverter;
import acs.data.converters.ElementConverter;
import acs.data.dal.ActiveElementDataAccessRepository;

@Component
public class ActionGetAllClientOrders implements ActionStrategy {
	private ActionConverter actionConverter;
	private ElementConverter elementConverter;
	private ActiveElementDataAccessRepository elementDataAccessRepository;

	@Autowired
	public ActionGetAllClientOrders(ActionConverter actionConverter, ElementConverter elementConverter, ActiveElementDataAccessRepository elementDataAccessRepository) {
		super();
		this.actionConverter = actionConverter;
		this.elementConverter = elementConverter;
		this.elementDataAccessRepository = elementDataAccessRepository;
	}

	@Override
	public Object doAction(ActionEntity getAllClientOrdersAction) {
		return this.elementDataAccessRepository
				.findAllPlayerActiveElements(this.actionConverter.stringToElement(getAllClientOrdersAction.getElement()).getElementId(), getAllClientOrdersAction.getInvokedBy(), true)
				.stream()
				.map(this.elementConverter::fromEntity)
				.collect(Collectors.toList()).toArray(new ElementBoundary[0]);
	}

	@Override
	public String getActionType() {
		return "Get All Client Orders";
	}

}
