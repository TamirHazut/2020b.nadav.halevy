package acs.logic.order;

import acs.data.ActionEntity;

public interface ActionStrategy {
	
	public Object doAction(ActionEntity action);
	
	public String getActionType();

}
