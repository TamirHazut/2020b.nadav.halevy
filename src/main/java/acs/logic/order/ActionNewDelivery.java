package acs.logic.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import acs.data.ActionEntity;
import acs.data.ElementEntity;
import acs.data.converters.ActionConverter;
import acs.data.converters.ElementConverter;
import acs.data.dal.ElementDataAccessRepository;
import acs.logic.ElementId;
import acs.logic.Exceptions.DataInvalidException;
import acs.logic.Exceptions.ElementNotFoundException;

@Component
public class ActionNewDelivery implements ActionStrategy {
	private ActionConverter actionConverter;
	private ElementDataAccessRepository elementDataAccessRepository;
	private ElementConverter elementConverter;

	@Autowired
	public ActionNewDelivery(ActionConverter actionConverter, ElementDataAccessRepository elementDataAccessRepository, ElementConverter elementConverter) {
		super();
		this.actionConverter = actionConverter;
		this.elementDataAccessRepository = elementDataAccessRepository;
		this.elementConverter = elementConverter;
	}

	// manager selects an order
	// manager press new delivery on order selected
	// a pop-up window opens to choose the delivery person
	// the client sends an user update for the manager with type PLAYER
	// the client sends an invokeAction with type 'New Delivery'
	// the client sends an user update for the manager with type MANAGER
	@Override
	public Object doAction(ActionEntity newDelivery) {
		if (!newDelivery.getActionAttributes().containsKey("deliveryPerson")) {
			throw new DataInvalidException("No delivery person found");
		}
		ElementId orderId = this.actionConverter.stringToElement(newDelivery.getElement()).getElementId();
		ElementEntity order = this.elementDataAccessRepository.findById(orderId)
				.orElseThrow(() -> new ElementNotFoundException(orderId.getId()));

		ElementId deliveryPersonId = this.elementConverter
				.stringToElementId(newDelivery.getActionAttributes().get("deliveryPerson").toString());
		ElementEntity deliveryPerson = this.elementDataAccessRepository.findById(deliveryPersonId)
				.orElseThrow(() -> new ElementNotFoundException(deliveryPersonId.getId()));

		if (deliveryPerson.getElementAttributes().get("WORKING_STATUS").toString().equals("false")) {
			throw new DataInvalidException(deliveryPerson.getElementId().getId() + " is not working");
		}
		if (deliveryPerson.getElementAttributes().get("ON_DELIVERY").toString().equals("true")) {
			throw new DataInvalidException(deliveryPerson.getElementId().getId() + " is already in a delivery");
		}

		order.getElementAttributes().replace("status", "DELIVERY_ON_THE_WAY");
		order.getElementAttributes().put("deliveryPerson",
				newDelivery.getActionAttributes().get("deliveryPerson").toString());
		order.setLocation(deliveryPerson.getLocation());

		deliveryPerson.getElementAttributes().replace("ON_DELIVERY", "true");

		deliveryPerson.bindParentToChild(order);
		
		this.elementDataAccessRepository.save(deliveryPerson);
		return this.elementConverter.fromEntity(this.elementDataAccessRepository.save(order));
	}

	@Override
	public String getActionType() {
		return "New Delivery Order";
	}

}
