package acs.logic.order;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import acs.data.ActionEntity;
import acs.data.ElementEntity;
import acs.data.converters.ActionConverter;
import acs.data.converters.ElementConverter;
import acs.data.dal.ElementDataAccessRepository;
import acs.logic.ElementId;
import acs.logic.Exceptions.DataInvalidException;
import acs.logic.Exceptions.ElementNotFoundException;

@Component
public class ActionNewOrder implements ActionStrategy {
	private ActionConverter actionConverter;
	private ElementConverter elementConverter;
	private ElementDataAccessRepository elementDataAccessRepository;

	@Autowired
	public ActionNewOrder(ActionConverter actionConverter, ElementConverter elementConverter, ElementDataAccessRepository elementDataAccessRepository) {
		super();
		this.actionConverter = actionConverter;
		this.elementConverter = elementConverter;
		this.elementDataAccessRepository = elementDataAccessRepository;
	}

	@Override
	public Object doAction(ActionEntity newOrderAction) {
		if (!newOrderAction.getActionAttributes().containsKey("address")
				|| !newOrderAction.getActionAttributes().containsKey("content")) {
			throw new DataInvalidException("Address or Content is not valid");
		}
		ElementId restaurantId = this.actionConverter.stringToElement(newOrderAction.getElement()).getElementId();
		ElementEntity restaurant = this.elementDataAccessRepository.findById(restaurantId)
				.orElseThrow(() -> new ElementNotFoundException(restaurantId.getId()));

		ElementEntity order = new ElementEntity();
		order.setElementId(new ElementId(restaurantId.getDomain(), UUID.randomUUID().toString()));
		order.setActive(true);
		order.setType("order");
		order.setActive(true);
		order.setLocation(restaurant.getLocation());
		order.setElementAttributes(newOrderAction.getActionAttributes());
		order.getElementAttributes().put("status", "IN_ORDER");
		order.getElementAttributes().put("client", newOrderAction.getInvokedBy());
		order.setCreatedBy(restaurant.getCreatedBy());
		order.setCreatedTimestamp(new Date());
		String name = "New Order";
		if (newOrderAction.getActionAttributes().containsKey("name")) {
			name = newOrderAction.getActionAttributes().get("name").toString();
		}
		order.setName(name);
		restaurant.bindParentToChild(order);

		this.elementDataAccessRepository.save(restaurant);
		return this.elementConverter.fromEntity(this.elementDataAccessRepository.save(order));
	}

	@Override
	public String getActionType() {
		return "New Order";
	}

}
