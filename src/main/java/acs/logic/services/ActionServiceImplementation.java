package acs.logic.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import acs.aop.MyLogger;
import acs.boundaries.ActionBoundary;
import acs.data.ActionEntity;
import acs.data.converters.ActionConverter;
import acs.data.dal.ActionDataAccessRepository;
import acs.logic.ActionId;
import acs.logic.ActionService;
import acs.logic.CheckInputsService;
import acs.logic.Exceptions.DataInvalidException;
import acs.logic.Exceptions.ElementNotFoundException;
import acs.logic.Exceptions.UnauthorizedUserException;
import acs.logic.order.ActionResolver;

@Service
public class ActionServiceImplementation implements ActionService {
	private ActionConverter actionConverter;
	private CheckInputsService checker;
	private ActionDataAccessRepository actionDataAccessRepository;
	private ActionResolver actionResolver;

	public ActionServiceImplementation() {

	}

	@Autowired
	public ActionServiceImplementation(ActionDataAccessRepository actionDataAccessRepository,
			CheckInputsService checker, ActionConverter converter, ActionResolver actionResolver) {
		super();
		this.actionDataAccessRepository = actionDataAccessRepository;
		this.checker = checker;
		this.actionConverter = converter;
		this.actionResolver = actionResolver;
	}

	@Override
	@Transactional
	@MyLogger
	public Object invokeAction(ActionBoundary action) {
		checker.email_Validation_check(action.getInvokedBy().getUserId().getEmail());
		checker.element_Validation_check(action.getElement().toString());
		checker.invokedBy_Validation_check(action.getInvokedBy().toString());

		if (!checker.isPlayer(action.getInvokedBy().getUserId().getDomain(),
				action.getInvokedBy().getUserId().getEmail()))
			throw new UnauthorizedUserException(action.getInvokedBy().getUserId().getEmail());
		if (!checker.isElementActive(action.getElement().getElementId()))
			throw new ElementNotFoundException(action.getElement().getElementId().getId());
		if (action.getActionId() != null)
			throw new DataInvalidException("actionId must be null");

		action.setActionId(new ActionId(action.getInvokedBy().getUserId().getDomain(), UUID.randomUUID().toString()));
		action.setCreatedTimestamp(new Date());
		ActionEntity actionEntity = actionConverter.boundaryToEntity(action);
		this.actionDataAccessRepository.save(actionEntity);
		Object o = actionResolver.doAction(actionEntity);
		return o;
	}

	@Override
	@Transactional(readOnly = true)
	@MyLogger
	public List<ActionBoundary> getAllActions(String adminDomain, String adminEmail, int size, int page) {
		if (!this.checker.isAdmin(adminDomain, adminEmail)) {
			throw new UnauthorizedUserException(adminEmail);
		}
		Iterable<ActionEntity> all = this.actionDataAccessRepository
				.findAll(PageRequest.of(page, size, Direction.DESC, "actionId"));
		List<ActionBoundary> rv = new ArrayList<>();
		for (ActionEntity action : all) {
			// map entities to boundaries
			rv.add(this.actionConverter.entityToBoundary(action));
		}
		return rv;
	}

	@Override
	@Transactional
	@MyLogger
	public void deleteAllActions(String adminDomain, String adminEmail) {
		if (!this.checker.isAdmin(adminDomain, adminEmail)) {
			throw new UnauthorizedUserException(adminEmail);
		}
		this.actionDataAccessRepository.deleteAll();
	}
}
