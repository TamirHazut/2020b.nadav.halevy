package acs.logic.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import acs.aop.MyLogger;
import acs.boundaries.UserBoundary;
import acs.data.UserEntity;
import acs.data.converters.UserConverter;
import acs.data.dal.UserDataAccessRepository;
import acs.logic.UserService;
import acs.logic.Exceptions.DataInvalidException;
import acs.logic.Exceptions.UnauthorizedUserException;
import acs.logic.Exceptions.UserNotFoundException;
import acs.logic.CheckInputsService;
import acs.logic.UserId;

@Service
public class UserServiceImplementation implements UserService {
	private UserDataAccessRepository users;
	private UserConverter userConv;
	private CheckInputsService checker;
	private String domain;

	public UserServiceImplementation() {
	}

	@Autowired
	public UserServiceImplementation(UserConverter userConv, UserDataAccessRepository users,CheckInputsService checker) {
		super();
		this.userConv = userConv;
		this.users = users;
		this.checker = checker;
	}

	@Value("${spring.application.name:acs}")
	public void setDomain(String domain) {
		this.domain = domain;
	}

	@Override
	@MyLogger
	public UserBoundary createUser(UserBoundary user) {
		if (user == null) {
			throw new DataInvalidException("Some of the data that was entered is invalid");
		}
		checker.email_Validation_check(user.getUserId().getEmail());
		checker.username_Validation_check(user.getUsername());
		checker.role_Validation_check(user.getRole());
		checker.avatar_Validation_check(user.getAvatar());
		user.getUserId().setDomain(domain);
		UserEntity retval = users.save(userConv.boundaryToEntity(user));
		return this.userConv.entityToBoundary(retval);
	}

	@Transactional(readOnly = true)
	@MyLogger
	public UserBoundary login(String userDomain, String userEmail) {
		UserId id = new UserId(userDomain, userEmail);
		Optional<UserEntity> user = users.findById(id);
		if (!user.isPresent()) {
			throw new UserNotFoundException("User with the id: " + id + " is not in the system");
		}
		return userConv.entityToBoundary(user.get());
	}

	@Override
	@MyLogger
	public UserBoundary updateUser(String userDomain, String userEmail, UserBoundary update) {
		UserId userID = new UserId(userDomain, userEmail);
		if (update == null || update.getUserId() == null) {
			throw new DataInvalidException("Some of the data that was entered is invalid");
		}
		if (!update.getUserId().equals(userID)) {
			update.setUserId(userID);
		}
		UserBoundary userFromDB = userConv.entityToBoundary(users.findById(userID).orElseThrow(() -> new UserNotFoundException(userEmail)));
		if(update.getAvatar() != null && (update.getAvatar() != userFromDB.getAvatar())) {
			userFromDB.setAvatar(update.getAvatar());
		}
		
		if(update.getRole() != null && (update.getRole() != userFromDB.getRole())) {
			userFromDB.setRole(update.getRole());
		}
		if(update.getUsername() != null && (update.getUsername() != userFromDB.getUsername())) {
			userFromDB.setUsername(update.getUsername());
		}
		return userConv.entityToBoundary(users.save(userConv.boundaryToEntity(userFromDB)));
	}

	@Transactional(readOnly = true)
	@MyLogger
	public List<UserBoundary> getAllUsers(String adminDomain, String adminEmail, int size, int page) {
		if (!checker.isAdmin(adminDomain, adminEmail)) {
			throw new UnauthorizedUserException(adminEmail);
		}
		return users
				.findAll(PageRequest.of(page, size, Direction.DESC, "userId"))
				.stream()
				.map(this.userConv::entityToBoundary)
				.collect(Collectors.toList());
	}

	@Override
	@MyLogger
	public void deleteAllUsers(String adminDomain, String adminEmail) {
		if (!checker.isAdmin(adminDomain, adminEmail)) {
			throw new UnauthorizedUserException(adminEmail);
		}
		users.deleteAll();
	}


}
