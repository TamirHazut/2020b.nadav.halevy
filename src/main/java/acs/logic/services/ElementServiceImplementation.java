package acs.logic.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import acs.aop.MyLogger;
import acs.boundaries.ElementBoundary;
import acs.boundaries.ElementIdBoundary;
import acs.data.ElementEntity;
import acs.data.converters.ElementConverter;
import acs.data.dal.ActiveElementDataAccessRepository;
import acs.data.dal.UserDataAccessRepository;
import acs.logic.CheckInputsService;
import acs.logic.CreatedBy;
import acs.logic.ElementId;
import acs.logic.EnhancedElementService;
import acs.logic.UserId;
import acs.logic.Exceptions.DataInvalidException;
import acs.logic.Exceptions.ElementNotFoundException;
import acs.logic.Exceptions.UnauthorizedUserException;

@Service
public class ElementServiceImplementation implements EnhancedElementService {
	private ElementConverter converter;
	private ActiveElementDataAccessRepository elementDataAccessRepository;
	private CheckInputsService checker;
	private String domain;

	public ElementServiceImplementation() {

	}

	@Autowired
	public ElementServiceImplementation(ActiveElementDataAccessRepository elementDataAccessRepository,UserDataAccessRepository userDAL,
			ElementConverter converter,CheckInputsService checker) {
		super();
		this.elementDataAccessRepository = elementDataAccessRepository;
		this.converter = converter;
		this.checker = checker;
	}

	protected String getDomain() {
		return domain;
	}

	@Value("${spring.application.name:acs}")
	public void setDomain(String domain) {
		this.domain = domain;
	}

	@Transactional
	@MyLogger
	public ElementBoundary create(String managerDomain, String managerEmail, ElementBoundary element) {
		if (!this.checker.isManager(managerDomain, managerEmail)){ //
			throw new UnauthorizedUserException(managerEmail);
		}
		if (isBoundaryValid(element)) {
			ElementEntity entity = this.converter.toEntity(element);
			// server updates
			entity.setElementId((new ElementId(this.domain, UUID.randomUUID().toString())));
			entity.setCreatedTimestamp(new Date());
			entity.setCreatedBy(this.converter.createdByToString(new CreatedBy(new UserId(managerDomain, managerEmail))));
			if (element.getActive() == null) {
				entity.setActive(true);
			}
			// save to DB
			return converter.fromEntity(this.elementDataAccessRepository.save(entity));

		} else {
			throw new RuntimeException("input is not valid!");
		}
	}

	private boolean isBoundaryValid(ElementBoundary element) {
		boolean retval = element.getCreatedBy() != null 
				&& element.getLocation() != null 
				&& element.getName() != null;
		this.checker.type_Validation_check(element.getType());
		return retval;
	}

	@Transactional
	@MyLogger
	public ElementBoundary update(String managerDomain, String managerEmail, String elementDomain, String elementId,
			ElementBoundary update) {
		if (!this.checker.isManager(managerDomain, managerEmail)) {
			throw new UnauthorizedUserException(managerEmail);
		}
		if (isChanged(elementDomain, elementId, update)) {
			throw new DataInvalidException("Some of the data entered is invalid.");
		}
		ElementId eId = new ElementId(elementDomain, elementId);
		ElementEntity entity = this.elementDataAccessRepository.findById(eId)
				.orElseThrow(() -> new ElementNotFoundException("could not find object by id: " + elementId));
		if (update.getActive() != null && update.getActive() != entity.isActive()) {
			entity.setActive(update.getActive());
		}
		if (update.getLocation() != null) {
			if (!update.getLocation().equals(entity.getLocation())) {

				entity.setLocation(update.getLocation());
			}
		}
		if (update.getType() != null) {
			if (!update.getType().isEmpty() && !update.getType().equals(entity.getType())) {
				entity.setType(update.getType());
			}
		}
		if (update.getName() != null) {
			if (!update.getName().isEmpty() && !update.getName().equals(entity.getName())) {
				entity.setName(update.getName());
			}
		}
		if (update.getElementAttributes() != null) {
			if (!update.getElementAttributes().isEmpty() && !update.getElementAttributes().equals(entity.getElementAttributes())) {
				entity.setElementAttributes(update.getElementAttributes());
			}
		}
		return converter.fromEntity(this.elementDataAccessRepository.save(entity));
	}

	private boolean isChanged(String elementDomain, String elementId, ElementBoundary update) {
		boolean retval = false;
		ElementId eId = new ElementId(elementDomain, elementId);
		ElementEntity entity = this.elementDataAccessRepository.findById(eId)
				.orElseThrow(() -> new ElementNotFoundException("could not find object by id: " + elementId));
		retval = ((update.getElementId() == null || !entity.getElementId().equals(this.converter.toEntity(update.getElementId())))
				&& (!entity.getCreatedTimestamp().equals(update.getCreatedTimestamp()) || update.getCreatedTimestamp() == null)
				&& (update.getCreatedBy() == null || converter.stringToCreatedBy(entity.getCreatedBy()) != update.getCreatedBy()));
		return retval;

	}

	@Transactional(readOnly = true)
	@MyLogger
	public List<ElementBoundary> getAll(String userDomain, String userEmail) {
		if (!this.checker.isManager(userDomain, userEmail)) {
			throw new UnauthorizedUserException(userEmail);
		}
		Iterable<ElementEntity> all = this.elementDataAccessRepository.findAll();
		List<ElementBoundary> rv = new ArrayList<>();
		for (ElementEntity entity : all) {
			// map entities to boundaries
			rv.add(this.converter.fromEntity(entity));
		}

		return rv;

	}

	@Transactional(readOnly = true)
	@MyLogger
	public ElementBoundary getSpecificElement(String userDomain, String userEmail, String elementDomain,
			String elementId) {
		if (!this.checker.isUser(userDomain, userEmail) || this.checker.isAdmin(userDomain, userEmail)) {
			throw new UnauthorizedUserException(userEmail);
		}
		Optional<ElementEntity> entity = this.elementDataAccessRepository
				.findById(this.converter.stringsToElementId(elementDomain, elementId));
		if (entity.isPresent()) {
			return this.converter.fromEntity(entity.get());

			// commit transaction
		} else {
			throw new ElementNotFoundException("could not find object by id: " + elementId);
		}
	}

	@Transactional
	@MyLogger
	public void deleteAllElement(String adminDomain, String adminEmail) {
		if(!checker.isAdmin(adminDomain, adminEmail)) {
			throw new UnauthorizedUserException(adminEmail);
		}
		this.elementDataAccessRepository.deleteAll();
	}


	@Transactional
	@MyLogger
	public void bindParentToChild(ElementIdBoundary childId, String managerDomain, String managerEmail,
			String elementDomain, String elementId) {
		if (!this.checker.isManager(managerDomain, managerEmail)) {
			throw new UnauthorizedUserException(managerEmail);
		}
		ElementEntity elementEntity = this.elementDataAccessRepository
				.findById(this.converter.stringsToElementId(elementDomain, elementId))
				.orElseThrow(() -> 
					new RuntimeException("could not find element by id:" + elementId + elementDomain));
		
		ElementEntity childEntity = this.elementDataAccessRepository
				.findById(this.converter.toEntity(childId))
				.orElseThrow(() -> 
					new RuntimeException("could not find child by id:" + childId.getId()));

		elementEntity.bindParentToChild(childEntity);
		this.elementDataAccessRepository.save(elementEntity);
		this.elementDataAccessRepository.save(childEntity);
		
	}


	@Transactional(readOnly = true)
	@MyLogger
	public List<ElementBoundary> getAllElementChildren(String userDomain, String userEmail, String elementDomain,
			String elementId, int size, int page) {
		if (!this.checker.isUser(userDomain, userEmail) || this.checker.isAdmin(userDomain, userEmail)) {
			throw new UnauthorizedUserException(userEmail);
		}
		if (this.checker.isPlayer(userDomain, userEmail)) {
			return this.elementDataAccessRepository
					.findAllByParentsAndActive(
							this.converter.stringsToElementId(elementDomain, elementId),
							true,
							PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
					.stream()
					.map(this.converter::fromEntity)
					.collect(Collectors.toList());
		} 
		return this.elementDataAccessRepository
				.findAllByParents(
						this.converter.stringsToElementId(elementDomain, elementId),
						PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
				.stream()
				.map(this.converter::fromEntity)
				.collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	@MyLogger
	public List<ElementBoundary> getAllElementParents(String userDomain, String userEmail, String elementDomain,
			String elementId, int size, int page) {
		if (!this.checker.isUser(userDomain, userEmail) || this.checker.isAdmin(userDomain, userEmail)) {
			throw new UnauthorizedUserException(userEmail);
		}
		if (this.checker.isPlayer(userDomain, userEmail)) {
			return this.elementDataAccessRepository
					.findAllByChildrenAndActive(
							this.converter.stringsToElementId(elementDomain, elementId),
							true,
							PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
					.stream()
					.map(this.converter::fromEntity)
					.collect(Collectors.toList());
		} 
		return this.elementDataAccessRepository
				.findAllByChildren(
						this.converter.stringsToElementId(elementDomain, elementId),
						PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
				.stream()
				.map(this.converter::fromEntity)
				.collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	@MyLogger
	public List<ElementBoundary> getAllElements(String userDomain, String userEmail, int size, int page) {
		if (!this.checker.isUser(userDomain, userEmail) || this.checker.isAdmin(userDomain, userEmail)) {
			throw new UnauthorizedUserException(userEmail);
		}
		if (this.checker.isPlayer(userDomain, userEmail)) {
			return this.elementDataAccessRepository
					.findByActive(true, PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
					.stream()
					.map(this.converter::fromEntity)
					.collect(Collectors.toList());
		} 
		return this.elementDataAccessRepository
				.findAll(PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
				.stream()
				.map(this.converter::fromEntity)
				.collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	@MyLogger
	public List<ElementBoundary> getAllElementsByName(String userDomain, String userEmail, String name, int size,
			int page) {
		if (!this.checker.isUser(userDomain, userEmail) || this.checker.isAdmin(userDomain, userEmail)) {
			throw new UnauthorizedUserException(userEmail);
		}
		if (this.checker.isPlayer(userDomain, userEmail)) {
			return this.elementDataAccessRepository
					.findByNameAndActive(name, true, PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
					.stream()
					.map(this.converter::fromEntity)
					.collect(Collectors.toList());
		} 
		return this.elementDataAccessRepository
				.findByName(
						name,
						PageRequest.of(page, size, Direction.DESC, "elementId._id"))
				.stream()
				.map(this.converter::fromEntity)
				.collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	@MyLogger
	public List<ElementBoundary> getAllElementsByType(String userDomain, String userEmail, String type, int size,
			int page) {
		if (!this.checker.isUser(userDomain, userEmail) || this.checker.isAdmin(userDomain, userEmail)) {
			throw new UnauthorizedUserException(userEmail);
		}
		if (this.checker.isPlayer(userDomain, userEmail)) {
			return this.elementDataAccessRepository
					.findByTypeAndActive(type, true, PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
					.stream()
					.map(this.converter::fromEntity)
					.collect(Collectors.toList());
		} 
		return this.elementDataAccessRepository
				.findByType(
						type,
						PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
				.stream()
				.map(this.converter::fromEntity)
				.collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	@MyLogger
	public List<ElementBoundary> getAllNearElements(String userDomain, String userEmail, double lat, double lng, double distance, int size,
			int page) {
		if (!this.checker.isUser(userDomain, userEmail) || this.checker.isAdmin(userDomain, userEmail)) {
			throw new UnauthorizedUserException(userEmail);
		}
		if (this.checker.isPlayer(userDomain, userEmail)) {
			return this.elementDataAccessRepository
					.findAllNearActiveElements(lat, lng, distance, true, PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
					.stream()
					.map(this.converter::fromEntity)
					.collect(Collectors.toList());
		} 
		return this.elementDataAccessRepository
				.findAllNearElements(
						lat, lng, distance,
						PageRequest.of(page, size, Direction.DESC, "name", "elementId._id"))
				.stream()
				.map(this.converter::fromEntity)
				.collect(Collectors.toList());
	}

}