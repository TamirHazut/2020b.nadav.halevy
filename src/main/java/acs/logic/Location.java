package acs.logic;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Location {
	/*
	  {
	  		"lat" : 1.1,
	  		"lng" : 2.2
	  }
	*/
	private @NonNull Double lat;
	private @NonNull Double lng;
}
