package acs.logic;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ElementId implements Comparable<ElementId>, Serializable {
	/*
	 * { "domain" : "domain", "id: 	: "id" }
	 */
	private @NonNull String domain;
	private @NonNull String id;
	private static final long serialVersionUID = 1L;

	@Override
	public int compareTo(ElementId o) {
		if (o == null)
			return 1;

		if (this.domain.compareTo(o.domain) == 0)
			return this.id.compareTo(o.id);

		return this.domain.compareTo(o.domain);
	}
}
