package acs.logic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import acs.data.ElementEntity;
import acs.data.UserEntity;
import acs.data.UserRole;
import acs.data.dal.ActiveElementDataAccessRepository;
import acs.data.dal.ElementDataAccessRepository;
import acs.data.dal.UserDataAccessRepository;
import acs.logic.Exceptions.DataInvalidException;
import acs.logic.Exceptions.ElementNotFoundException;
import acs.logic.Exceptions.UserNotFoundException;

@Component
public class CheckInputsServiceImplementation implements CheckInputsService {
	private UserDataAccessRepository userDAL;
	private ElementDataAccessRepository elementDAL;

	public CheckInputsServiceImplementation() {
	}

	@Autowired
	public CheckInputsServiceImplementation(UserDataAccessRepository userDAL, ActiveElementDataAccessRepository elementDAL) {
		super();
		this.setUserDAL(userDAL);
		this.setElementDAL(elementDAL);
	}

	private UserEntity getUserFromUserService(String userDomain, String userEmail) {
		UserId userId = new UserId(userDomain, userEmail);
		return this.userDAL.findById(userId).orElseThrow(() -> new UserNotFoundException(userEmail));
	}

	private ElementEntity getElementFromElementService(String elementDomain, String id) {
		ElementId elementId = new ElementId(elementDomain, id);
		return this.elementDAL.findById(elementId).orElseThrow(() -> new ElementNotFoundException(id));
	}

	public boolean isAdmin(String adminDomain, String adminEmail) {
		UserEntity admin = getUserFromUserService(adminDomain, adminEmail);
		return admin.getRole().equals(UserRole.ADMIN);
	}

	public boolean isManager(String managerDomain, String managerEmail) {
		UserEntity manager = getUserFromUserService(managerDomain, managerEmail);
		return manager.getRole().equals(UserRole.MANAGER);
	}

	public boolean isPlayer(String playerDomain, String playerEmail) {
		UserEntity player = getUserFromUserService(playerDomain, playerEmail);
		return player.getRole().equals(UserRole.PLAYER);
	}

	public boolean isUser(String userDomain, String userEmail) {
		UserEntity user = getUserFromUserService(userDomain, userEmail);
		return user != null;
	}

	public boolean isElementActive(ElementId elementId) {
		return getElementFromElementService(elementId.getDomain(), elementId.getId()).isActive();
	}

	public void email_Validation_check(String email) {
		if (email == null)
			throw new DataInvalidException("invalid Email");

		String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(email);
		if (!matcher.matches())
			throw new DataInvalidException("invalid Email");
	}

	public void role_Validation_check(UserRole role) {
		if (role == null)
			throw new DataInvalidException("invalid Role");

		for (UserRole user_role : UserRole.values())
			if (user_role == role)
				return;
		throw new DataInvalidException("invalid Role");
	}

	public void username_Validation_check(String username) {
		if (!string_Validation_check(username))
			throw new DataInvalidException("invalid Username");
	}

	public void avatar_Validation_check(String avatar) {
		if (!string_Validation_check(avatar))
			throw new DataInvalidException("invalid Avatar");
	}

	public void type_Validation_check(String type) {
		if (!string_Validation_check(type))
			throw new DataInvalidException("invalid Type");
	}

	public void invokedBy_Validation_check(String invokedBy) {
		if (!string_Validation_check(invokedBy))
			throw new DataInvalidException("invalid invokedBy User");
	}

	public void element_Validation_check(String element) {
		if (!string_Validation_check(element))
			throw new DataInvalidException("invalid Element");
	}

	private boolean string_Validation_check(String str) {
		if (str == null)
			return false;

		if (str.length() <= 0)
			return false;

		return true;
	}

	public UserDataAccessRepository getUserDAL() {
		return userDAL;
	}

	public void setUserDAL(UserDataAccessRepository userDAL) {
		this.userDAL = userDAL;
	}

	public ElementDataAccessRepository getElementDAL() {
		return elementDAL;
	}

	public void setElementDAL(ElementDataAccessRepository elementDAL) {
		this.elementDAL = elementDAL;
	}

}
