package acs.logic;

import java.util.List;

import acs.boundaries.ElementBoundary;
import acs.boundaries.ElementIdBoundary;

public interface EnhancedElementService extends ElementService{
	public void bindParentToChild(ElementIdBoundary child, String managerDomain, String managerEmail, String elementDomain, String elementId);
	public List<ElementBoundary> getAllElementChildren(String userDomain, String userEmail, String elementDomain, String elementId, int size, int page);
	public List<ElementBoundary> getAllElementParents(String userDomain, String userEmail, String elementDomain, String elementId, int size, int page);
	public List<ElementBoundary> getAllElements(String userDomain, String userEmail, int size, int page);
	public List<ElementBoundary> getAllElementsByName(String userDomain, String userEmail, String name, int size, int page);
	public List<ElementBoundary> getAllElementsByType(String userDomain, String userEmail, String type, int size, int page);
	public List<ElementBoundary> getAllNearElements(String userDomain, String userEmail, double lng, double lat, double distance, int size, int page);
}
