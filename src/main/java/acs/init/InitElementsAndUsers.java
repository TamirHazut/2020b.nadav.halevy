package acs.init;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import acs.boundaries.ActionBoundary;
import acs.boundaries.ElementBoundary;
import acs.boundaries.UserBoundary;
import acs.data.UserRole;
import acs.logic.ActionService;
import acs.logic.CreatedBy;
import acs.logic.Element;
import acs.logic.ElementId;
import acs.logic.EnhancedElementService;
import acs.logic.InvokedBy;
import acs.logic.Location;
import acs.logic.UserId;
import acs.logic.UserService;

@Component
@Profile("manualTests")
public class InitElementsAndUsers implements CommandLineRunner{
	private EnhancedElementService elementService;
	private UserService userService;
	private ActionService actionService;
	
	
	@Autowired	
	public InitElementsAndUsers(EnhancedElementService elementService,UserService userService, ActionService actionService) {
		this.elementService = elementService;
		this.userService = userService;
		this.actionService = actionService;
	}
	
	@Override
	public void run(String... args) throws Exception {
		
		// Clear database
		UserBoundary admin1 = new UserBoundary(new UserId("admin1@init.test"), UserRole.ADMIN, "admin1", ":-)");
		admin1 = this.userService.createUser(admin1);
		this.elementService.deleteAllElement(admin1.getUserId().getDomain(), admin1.getUserId().getEmail());
		this.actionService.deleteAllActions(admin1.getUserId().getDomain(), admin1.getUserId().getEmail());
		this.userService.deleteAllUsers("2020b.nadav.halevy", "admin1@init.test");
		
		// Clean start
		UserBoundary manager1 = new UserBoundary(new UserId("manager1@init.test"), UserRole.MANAGER, "manager1", ";)");
		UserBoundary player1 = new UserBoundary(new UserId("player1@init.test"), UserRole.PLAYER, "player1", ";)");
		admin1 = this.userService.createUser(admin1);
		manager1 = this.userService.createUser(manager1);
		player1 = this.userService.createUser(player1);

		// create a restaurant
		ElementBoundary restaurant = new ElementBoundary(null, "restaurant", "rest", true, null,
				new CreatedBy(manager1.getUserId()), new Location(34.72, 32.2), null);
		restaurant = this.elementService
				.create(manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), restaurant);
		
		// create a delivery person
		Map<String, Object> deliveryPersonElementAttributes = new HashMap<String, Object>();
		deliveryPersonElementAttributes.put("WORKING_STATUS", "true");
		deliveryPersonElementAttributes.put("ON_DELIVERY", "false");
		ElementBoundary deliveryPerson1 = new ElementBoundary(null, "Delivery Person", "Delivery Person 1", true, null,
				new CreatedBy(manager1.getUserId()), new Location(34.72, 32.2), deliveryPersonElementAttributes);
		deliveryPerson1 = this.elementService
				.create(manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), deliveryPerson1);
		
		ElementBoundary deliveryPerson2 = new ElementBoundary(null, "Delivery Person", "Delivery Person 2", true, null,
				new CreatedBy(manager1.getUserId()), new Location(34.72, 32.2), deliveryPersonElementAttributes);
		deliveryPerson2 = this.elementService
				.create(manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), deliveryPerson2);
		
		ElementBoundary deliveryPerson3 = new ElementBoundary(null, "Delivery Person", "Delivery Person 3", true, null,
				new CreatedBy(manager1.getUserId()), new Location(34.72, 32.2), deliveryPersonElementAttributes);
		deliveryPerson3 = this.elementService
				.create(manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), deliveryPerson3);
		
		// add child delivery person to restaurant
		this.elementService.bindParentToChild(deliveryPerson1.getElementId(), manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), restaurant.getElementId().getDomain(), restaurant.getElementId().getId());
		this.elementService.bindParentToChild(deliveryPerson2.getElementId(), manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), restaurant.getElementId().getDomain(), restaurant.getElementId().getId());
		this.elementService.bindParentToChild(deliveryPerson3.getElementId(), manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), restaurant.getElementId().getDomain(), restaurant.getElementId().getId());
		
		// create an order
		Map<String, Object> orderAttributes = new HashMap<String, Object>();
		orderAttributes.put("address", "050");
		orderAttributes.put("content", "content");
		orderAttributes.put("name", "order1");
		ActionBoundary newOrderAction = new ActionBoundary(null, "New Order",
				new Element(new ElementId(restaurant.getElementId().getDomain(), restaurant.getElementId().getId())), null, new InvokedBy(player1.getUserId()), orderAttributes);
		this.actionService.invokeAction(newOrderAction);
		
		// change manager role to PLAYER
		manager1.setRole(UserRole.PLAYER);
		manager1 = this.userService.updateUser(manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), manager1);
		
		// create a delivery
		ActionBoundary newDeliveryOrderAction = new ActionBoundary(null, "New Order",
				new Element(new ElementId(restaurant.getElementId().getDomain(), restaurant.getElementId().getId())), null, new InvokedBy(player1.getUserId()), orderAttributes);
		orderAttributes.replace("name", "delivery1");
		this.actionService.invokeAction(newDeliveryOrderAction);
		List<ElementBoundary> orderList = this.elementService.getAllElementsByName(manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), "delivery1", 1, 0);
		ElementBoundary order1 = orderList.get(0);
		newDeliveryOrderAction.setType("New Delivery Order");
		newDeliveryOrderAction.setInvokedBy(new InvokedBy(manager1.getUserId()));
		newDeliveryOrderAction.setElement(new Element(new ElementId(order1.getElementId().getDomain(), order1.getElementId().getId())));
		orderAttributes.put("deliveryPerson", deliveryPerson1.getElementId().getDomain() + "," + deliveryPerson1.getElementId().getId());
		newDeliveryOrderAction.setActionId(null);
		this.actionService.invokeAction(newDeliveryOrderAction);
		
		// receive a delivery
		ActionBoundary receiveDeliveryAction = new ActionBoundary(null, "New Order",
				new Element(new ElementId(restaurant.getElementId().getDomain(), restaurant.getElementId().getId())), null, new InvokedBy(player1.getUserId()), orderAttributes);
		orderAttributes.replace("name", "received1");
		this.actionService.invokeAction(receiveDeliveryAction);
		orderList = this.elementService.getAllElementsByName(manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), "received1", 1, 0);
		ElementBoundary received1 = orderList.get(0);
		receiveDeliveryAction.setType("New Delivery Order");
		receiveDeliveryAction.setInvokedBy(new InvokedBy(manager1.getUserId()));
		receiveDeliveryAction.setElement(new Element(new ElementId(received1.getElementId().getDomain(), received1.getElementId().getId())));
		orderAttributes.put("deliveryPerson", deliveryPerson2.getElementId().getDomain() + "," + deliveryPerson2.getElementId().getId());
		receiveDeliveryAction.setActionId(null);
		this.actionService.invokeAction(receiveDeliveryAction);
		receiveDeliveryAction.setInvokedBy(new InvokedBy(player1.getUserId()));
		receiveDeliveryAction.setType("Delivery Received");
		receiveDeliveryAction.setActionId(null);
		this.actionService.invokeAction(receiveDeliveryAction);
		
		// cancel a delivery
		ActionBoundary cancelOrderAction = new ActionBoundary(null, "New Order",
		new Element(new ElementId(restaurant.getElementId().getDomain(), restaurant.getElementId().getId())), null, new InvokedBy(player1.getUserId()), orderAttributes);
		orderAttributes.replace("name", "cancel1");
		this.actionService.invokeAction(cancelOrderAction);
		orderList = this.elementService.getAllElementsByName(player1.getUserId().getDomain(), player1.getUserId().getEmail(), "cancel1", 1, 0);
		ElementBoundary cancel1 = orderList.get(0);
		cancelOrderAction.setType("Cancel Order");
		cancelOrderAction.setElement(new Element(new ElementId(cancel1.getElementId().getDomain(), cancel1.getElementId().getId())));
		cancelOrderAction.setActionId(null);
		this.actionService.invokeAction(cancelOrderAction);
		
		// cancel a delivery
		ActionBoundary cancelDeliveryAction = new ActionBoundary(null, "New Order",
		new Element(new ElementId(restaurant.getElementId().getDomain(), restaurant.getElementId().getId())), null, new InvokedBy(player1.getUserId()), orderAttributes);
		orderAttributes.replace("name", "cancel2");
		this.actionService.invokeAction(cancelDeliveryAction);
		orderList = this.elementService.getAllElementsByName(manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), "cancel2", 1, 0);
		ElementBoundary cancel2 = orderList.get(0);
		cancelDeliveryAction.setType("New Delivery Order");
		cancelDeliveryAction.setInvokedBy(new InvokedBy(manager1.getUserId()));
		cancelDeliveryAction.setElement(new Element(new ElementId(cancel2.getElementId().getDomain(), cancel2.getElementId().getId())));
		orderAttributes.put("deliveryPerson", deliveryPerson2.getElementId().getDomain() + "," + deliveryPerson2.getElementId().getId());
		cancelDeliveryAction.setActionId(null);
		this.actionService.invokeAction(cancelDeliveryAction);
		cancelDeliveryAction.setInvokedBy(new InvokedBy(player1.getUserId()));
		cancelDeliveryAction.setType("Cancel Order");
		cancelDeliveryAction.setElement(new Element(new ElementId(cancel2.getElementId().getDomain(), cancel2.getElementId().getId())));
		cancelDeliveryAction.setActionId(null);
		this.actionService.invokeAction(cancelDeliveryAction);

		// change manager role to MANAGER
		manager1.setRole(UserRole.MANAGER);
		manager1 = this.userService.updateUser(manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), manager1);
//		
//		this.elementService.bindParentToChild(order1.getElementId(), manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), restaurant.getElementId().getDomain(), restaurant.getElementId().getId());
//		this.elementService.bindParentToChild(order2.getElementId(), manager1.getUserId().getDomain(), manager1.getUserId().getEmail(), restaurant.getElementId().getDomain(), restaurant.getElementId().getId());
	}

}






