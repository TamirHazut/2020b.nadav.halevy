package acs.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import acs.boundaries.UserBoundary;
import acs.data.UserRole;
import acs.logic.ActionService;
import acs.logic.EnhancedElementService;
import acs.logic.UserId;
import acs.logic.UserService;

@Component
@Profile("clearStart")
public class ClearStart implements CommandLineRunner {
	private EnhancedElementService elementService;
	private UserService userService;
	private ActionService actionService;
	
	
	@Autowired	
	public ClearStart(EnhancedElementService elementService,UserService userService, ActionService actionService) {
		this.elementService = elementService;
		this.userService = userService;
		this.actionService = actionService;
	}

	@Override
	public void run(String... args) throws Exception {
		UserBoundary admin1 = new UserBoundary(new UserId("admin1@init.clearStart"), UserRole.ADMIN, "admin1", ":-)");
		admin1 = this.userService.createUser(admin1);
		UserId adminId = admin1.getUserId();
		this.actionService.deleteAllActions(adminId.getDomain(), adminId.getEmail());
		this.elementService.deleteAllElement(adminId.getDomain(), adminId.getEmail());
		this.userService.deleteAllUsers(adminId.getDomain(), adminId.getEmail());

	}

}
