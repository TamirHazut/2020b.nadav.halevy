package acs.aop;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggerAspect {
	
	private Log logger = LogFactory.getLog(LoggerAspect.class);
	
	@Around("@annotation(acs.aop.MyLogger)")
	public Object logAdviceProxy(ProceedingJoinPoint jp) throws Throwable {
		String methodName = jp.getSignature().getName();
		String targetClassName = jp.getTarget().getClass().getName(); // java reflection
		Object[] args = jp.getArgs();
		List<String> argsTypes = Stream.of(args)
				.map(arg->arg.getClass().getSimpleName() + " " + arg)
				.collect(Collectors.toList());
		String trace = new String("***********" + targetClassName + "." + methodName + "("+ argsTypes.toString().substring(1, argsTypes.toString().length()-1)+")");
		this.logger.debug(trace + "- start");
		try {
			Object rv = jp.proceed();
			this.logger.debug(trace + "- ended successfully");
			return rv;
		} catch (Throwable e) {
			this.logger.error(trace + " - ended with an error");
			throw e;
		}
	}
}







