import React, { useState, useMemo } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import elementService from './services/elementService';

import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/App.css';

import Login from './components/Login';
import Order from './components/Order';
import SignUp from './components/SignUp';
import AddRestaurant from './components/AddRestaurant';
import HomePage from './components/HomePage';
import MyOrders from './components/MyOrders';
import Error from './components/Error';
import Map from './components/Map'
import useGeolocation from 'react-hook-geolocation';
import { useEffect } from 'react';
import UserProfile from './components/UserProfile';
import { StyledUserName } from './styles/Style';


function App() {
  const [currentUser, setCurrentUser] = useState(null);
  const [restaurants, setRestaurants] = useState([]);

  const geolocation = useGeolocation();

  const location = useMemo(() => {
    if (!geolocation.error) {
      return { lat: geolocation.latitude, lng: geolocation.longitude }
    } else {
      return { lat: 0, lng: 0 }
    }
  }, [geolocation])

  useEffect(() => {
    if (currentUser) {
      elementService.getAllElementsByType(currentUser.userId.domain, currentUser.userId.email, 'restaurant')
        .then(response => { setRestaurants(response.data); })
        .catch(e => { console.log(e); });
    }
  }, [currentUser])

  return (
    <Router>
      <div>
        <nav className='navbar navbar-expand navbar-dark bg-dark'>
          <a href='/acs' className='navbar-brand'>TrackEat</a>
          {currentUser ?
            <div className='container-fluid'>
              <ul className='mr-auto navbar navbar-nav'>
                {currentUser.role === 'PLAYER' ?
                  <li className='nav-item'>
                    <Link to={'/acs/order'} className='nav-link'>Place Order</Link>
                  </li> : <div />}
                <li className='nav-item'>
                  <Link to={'/acs/myorders'} className='nav-link'>My Orders</Link>
                </li>
                <li className='nav-item'>
                  <Link to={'/acs/map'} className='nav-link'>Map</Link>
                </li>
                {currentUser.role === 'MANAGER' ?
                  <li className='nav-item'>
                    <Link to={'/acs/addrestaurant'} className='nav-link'>Add Restaurant</Link>
                  </li> : <div />}
              </ul>
              <ul className='navbar navbar-nav navbar-right navbar-dark bg-dark'>
                <li className='nav-item'>
                  <Link to={'/acs/profile'} className='nav-link'>
                    <StyledUserName>{currentUser.username}</StyledUserName>
                  </Link>
                </li>
              </ul>
            </div>
            :
            <div className='container-fluid'>
              <ul className='navbar navbar-nav navbar-expand navbar-right'>
                <li className='nav-item'>
                  <Link to={'/acs/signup'} className='nav-link'>Sign up</Link>
                </li>
                <li className='nav-item'>
                  <Link to={'/acs/users/login'} className='nav-link'>Login</Link>
                </li>
              </ul>
            </div>
          }
        </nav>

        <div className='container mt-3'>
          <Switch>
            <Route exact path={['/', '/acs']} component={HomePage} />
            <Route exact path='/acs/elements/:id' component={Order} />
            <Route exact path='/acs/map'>
              <Map location={location} elements={restaurants.map(restaurant => { return { 'name': restaurant.name, 'location': restaurant.location, 'color': 'red' } })} />
            </Route>
            <Route exact path='/acs/order'>
              <Order user={currentUser} />
            </Route>
            <Route exact path='/acs/error'>
              <Error />
            </Route>
            <Route exact path='/acs/addrestaurant'>
              <AddRestaurant user={currentUser} location={location} restaurants={restaurants.map(restaurant => { return { 'name': restaurant.name, 'location': restaurant.location, 'color': 'red' } })} />
            </Route>
            <Route exact path='/acs/myorders'>
              <MyOrders user={currentUser} location={location} restaurants={restaurants} />
            </Route>
            <Route exact path='/acs/signup' component={SignUp} />
            <Route exact path='/acs/Users/:id'>
              <Login setUser={setCurrentUser} />
            </Route>
            <Route exact patt='/acs/profile'>
              <UserProfile user={currentUser} setUser={setCurrentUser} />
            </Route>
          </Switch>
        </div>
      </div>
    </Router >
  );
}
export default App;