import React from "react";
import { MapWrapper } from "../styles/Style";
import GoogleMapReact from "google-map-react";
import Marker from "./Marker";

const options = { disableDefaultUI: false, disableDoubleClickZoom: true }
const dotenv = require('dotenv').config()
const api_key = process.env.REACT_APP_GOOGLE_MAPS_API_KEY
const zoom = 16;

function Map(props) {
    const handleClick = event => {
        if (props.onClick) {
            props.onClick(event);
        }
    }

    return (
        <MapWrapper >
            <GoogleMapReact bootstrapURLKeys={{ key: api_key, language: 'en' }} zoom={zoom} center={props.location} options={options} onClick={handleClick}>
                <Marker lat={props.location.lat} lng={props.location.lng} name={'You'} color={'blue'} />
                {props.elements ? props.elements.map((element, i) => <Marker key={i} lat={element.location.lat} lng={element.location.lng} name={element.name} color={element.color} />) : <Marker />}
            </GoogleMapReact>
        </MapWrapper>
    );
};

export default Map;
