import React from 'react';
import '../styles/Marker.css';

function Marker(props) {
    return (
        <div className="marker"
            style={{ backgroundColor: props.color, cursor: 'pointer' }}
            title={props.name}
        />
    )
};

export default Marker;