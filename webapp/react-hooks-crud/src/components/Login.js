import React, { useState } from 'react';
import { useHistory, Link } from 'react-router-dom';
import userDataService from '../services/userService';
import {OuterBoxedDiv, Button} from '../styles/Style';

function Login(props) {
  const [currentUserId, setCurrentUserId] = useState({ domain: '', email: ''});

  const history = useHistory();

  const getUser = () => {
    userDataService.get(currentUserId.domain, currentUserId.email)
      .then(response => {
        props.setUser(response.data);
        history.push('/acs/myorders');
      })
      .catch(e => {
        console.log(e);
        history.push('/acs/error', { error: e });
      });
  };

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCurrentUserId({ ...currentUserId, [name]: value });
  };

  return (
    <OuterBoxedDiv>
      <div className='Login-form '>
        <h4 className='text-center'>Login</h4>
        <form>
          <div className='form-group'>
            <label htmlFor='domain'>Domain</label>
            <input type='text' className='form-control' id='domain' name='domain' value={currentUserId.domain} onChange={handleInputChange} />
          </div>
          <div className='form-group'>
            <label htmlFor='email'>Email</label>
            <input type='text' className='form-control' id='email' name='email' value={currentUserId.email} onChange={handleInputChange} />
          </div>
        </form>
        <div>
          <div className='text-right'>
            <Link to={'/acs/add'}>Sign up</Link>
          </div>
          <div>
            <Button disabled={!currentUserId.domain || !currentUserId.email} onClick={getUser} className='btn btn-success'>Login</Button>
          </div>
        </div>
      </div>
    </OuterBoxedDiv>
  );
};

export default Login;