import React, { useState, useEffect } from "react";
import elementService from '../services/elementService';
import actionService from '../services/actionService';
import { useHistory } from 'react-router-dom';
import { OuterBoxedDiv, InnerBoxedDiv, Button, SelectBox, DefaultOption, SelectDiv } from "../styles/Style";

const order = {
	actionId: null,
	type: 'New Order',
	element: {
		elementId: {
			domain: '',
			id: ''
		}
	},
	invokedBy: {
		userId: {
			domain: '',
			email: ''
		}
	},
	actionAttributes: {
		address: '',
		content: '',
		name: ''
	}
}
function Order(props) {
	const [restaurants, setRestaurants] = useState([]);
	const [restaurant, setRestaurant] = useState({ index: null });
	const [orderAttributes, setOrderAttributes] = useState(order.actionAttributes);
	const history = useHistory();

	useEffect(() => {
		if (props.user) {
			order.element.elementId.domain = props.user.userId.domain;
			order.invokedBy.userId = props.user.userId;
			getAllElements();
		}
	}, [props.user]);

	useEffect(() => {
		if (restaurant.index) {
			order.element.elementId.id = restaurants[restaurant.index].elementId.id;
		}
	}, [restaurant, restaurants]);

	useEffect(() => {
		if (order.actionAttributes !== orderAttributes) {
			order.actionAttributes = orderAttributes;
		}
	}, [orderAttributes]);

	const submit = async () => {
		actionService.createNewAction(order).then(response => {
			props.setOrder(response.data);
			history.push('/acs/myorders');
		}).catch(e => { console.log(e); });
	};

	const changeRestaurant = event => {
		const { name, value } = event.target;
		setRestaurant({ ...restaurant, [name]: value });
	};

	const handleInputChange = event => {
		const { name, value } = event.target;
		setOrderAttributes({ ...orderAttributes, [name]: value });
	};

	const getAllElements = () => {
		elementService.getAllElementsByType(props.user.userId.domain, props.user.userId.email, 'restaurant').then(response => {
			setRestaurants(response.data);
		}).catch(e => { console.log(e); });
	};

	return (
		<OuterBoxedDiv>
			<form>
				<h4  className='text-center'>New Order</h4>
				<div>
					<SelectDiv>
						<label htmlFor="Restaurants">Restaurants</label>
						<SelectBox className="form-control" id="restaurantsList" required defaultValue={-1} value={restaurant.name} onChange={changeRestaurant} name="index">
							<DefaultOption value={-1} disabled>Select your option</DefaultOption>
							{restaurants.map((restaurant, i) => { return <option key={i} value={i}>{restaurant.name}</option> })};
	            		</SelectBox>
					</SelectDiv>
				</div>
				{restaurant.index ?
					<InnerBoxedDiv>
						<h4>Order Details</h4>
						<div>
							<div className="form-group">
								<label htmlFor="Name">Name</label>
								<input type="text" className="form-control" id="name" name="name" onChange={handleInputChange} />
							</div>
							<div className="form-group">
								<label htmlFor="Address">Address</label>
								<input type="text" className="form-control" id="address" name="address" onChange={handleInputChange} />
							</div>
							<div className="form-group">
								<label htmlFor="Content">Content</label>
								<input type="text" className="form-control" id="content" name="content" onChange={handleInputChange} />
							</div>
						</div>
						<Button disabled={!orderAttributes.name || !orderAttributes.address || !orderAttributes.content} onClick={submit} className="btn btn-success">Send Order </Button>
					</InnerBoxedDiv>
					: <div />}
			</form>
		</OuterBoxedDiv>
	);
};
export default Order;