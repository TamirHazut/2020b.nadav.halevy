import React, { useState, useEffect } from "react";
import elementService from '../services/elementService';
import actionService from '../services/actionService';
import userService from '../services/userService'
import { useHistory } from 'react-router-dom';
import Map from "./Map";
import { OuterBoxedDiv, InnerBoxedDiv, SelectBox, SelectDiv, DefaultOption, FlexDiv, InnerFlexDiv } from "../styles/Style";

const action = {
    actionId: null,
    type: '',
    element: {
        elementId: {
            domain: '',
            id: ''
        }
    },
    invokedBy: {
        userId: {
            domain: '',
            email: ''
        }
    },
    actionAttributes: {
    }
};

function MyOrders(props) {
    const [restaurant, setRestaurant] = useState({ index: null });
    const [elements, setElements] = useState([]);
    const [order, setOrder] = useState({ index: null });
    const [delivery, setDelivery] = useState({ index: null });
    const [deliveryPersons, setDeliveryPersons] = useState([]);
    const [deliveryPerson, setDeliveryPerson] = useState({ index: null });

    const history = useHistory();

    useEffect(() => {
        if (props.user) {
            action.element.elementId.domain = props.user.userId.domain;
            action.invokedBy.userId = props.user.userId;
        }
    }, [props.user]);

    useEffect(() => {
        if (restaurant.index) {
            getOrders();
            if (props.user.role === "MANAGER") {
                getDeliveryPersons();
            }
        }
    }, [restaurant, props.user]);

    const changeRestaurant = event => {
        const { name, value } = event.target;
        setRestaurant({ ...restaurant, [name]: value });
    };

    const changeOrder = event => {
        const { name, value } = event.target;
        setOrder({ ...order, [name]: value });
    };

    const changeDelivery = event => {
        const { name, value } = event.target;
        setDelivery({ ...delivery, [name]: value });
    };

    const changeDeliveryPerson = event => {
        const { name, value } = event.target;
        setDeliveryPerson({ ...deliveryPerson, [name]: value });
    };

    const getOrders = async () => {
        if (props.user.role === 'PLAYER') {
            action.element.elementId.id = props.restaurants[restaurant.index].elementId.id;
            action.type = "Get All Client Orders"
            actionService.createNewAction(action).then(
                response => {
                    setElements(response.data.filter(element => element.elementAttributes.status === "DELIVERY_ON_THE_WAY"));
                }
            )
        } else {
            elementService.getAllElementsByName(props.user.userId.domain, props.user.userId.email, props.restaurants[restaurant.index].name).then(
                response => {
                    const element = response.data[0].elementId;
                    const resManager = response.data[0].createdBy.userId;
                    elementService.getChildrenElements(resManager.domain, resManager.email, element.domain, element.id)
                        .then(response => setElements(response.data.filter(element => element.type === 'order')))
                }).catch(e => { console.log(e); });
        }
    }

    const getDeliveryPersons = () => {
        elementService.getAllElementsByType(props.user.userId.domain, props.user.userId.email, "Delivery Person")
            .then(response => {
                setDeliveryPersons(response.data);
            }).catch(e => { console.log(e); });
    };


    const submit = async () => {
        action.type = "Delivery Received";
        await actionService.createNewAction(action)
        history.push('/acs/end', { status: "accepted" });
    };

    const cancel = async () => {
        action.type = "Cancel Order";
        await actionService.createNewAction(action).then(res => {
            history.push('/acs/end', { status: "canceled" });
        }).catch(e => { history.push('/acs/error', { error: e }); })
    };

    const attach = async () => {
        action.type = 'New Delivery Order';
        action.element.elementId = order.element?.elementId;
        action.invokedBy.email = props.user.userId?.email;
        action.actionAttributes.deliveryPerson = deliveryPersons[deliveryPerson.index].elementId.domain + ',' + deliveryPersons[deliveryPerson.index].elementId.id;
        userService.changeManagerToPlayer(props.user.userId).then(resp1 => { actionService.createNewAction(action).then(resp => { userService.changePlayerToManager(props.user.userId); }) })
    }

    const showUserPage = () => {
        if (props.user.role === "MANAGER") {
            return (
                <InnerBoxedDiv>
                    <div className="form-group">
                        <FlexDiv>
                            <InnerFlexDiv>
                                <label htmlFor="Orders">Orders:</label>
                                <SelectBox className="form-control" id="ordersList" required defaultValue={-1} value={order.name} onChange={changeOrder} name="index">
                                    <DefaultOption disabled value={-1}>Select Order</DefaultOption>
                                    {elements
                                        .filter(order => order.elementAttributes.status === 'IN_ORDER')
                                        .map((order, i) => <option key={i} value={i}>{order.elementAttributes.name}</option>)
                                    };
	                            </SelectBox>
                                <br />
                                <SelectBox className="form-control" id="DeliveryPersonsList" required defaultValue={-1} value={deliveryPerson.name} onChange={changeDeliveryPerson} name="index">
                                    <DefaultOption disabled value={-1}>Select Delivery Man</DefaultOption>
                                    {deliveryPersons.map((deliveryPerson, i) => { return <option key={i} value={i}>{deliveryPerson.name}</option> })};
                                </SelectBox>
                                <br />
                                <button onClick={attach} disabled={!deliveryPerson.index || !order.index} className="btn btn-success">Attach</button>
                            </InnerFlexDiv>
                            <InnerFlexDiv>
                                <label htmlFor="Deliveries">Deliveries:</label>
                                <SelectBox className="form-control" id="order" required defaultValue={-1} value={delivery.name} onChange={changeDelivery} name="index">
                                    <DefaultOption disabled value={-1}>Select Delivery</DefaultOption>
                                    {elements
                                        .filter(delivery => delivery.elementAttributes.status === "DELIVERY_ON_THE_WAY")
                                        .map((delivery, i) => <option key={i} value={i}>{delivery.elementAttributes.name}</option>)
                                    };
	                            </SelectBox>
                            </InnerFlexDiv>
                        </FlexDiv>
                    </div>
                </InnerBoxedDiv>
            )
        }
        else {
            return (
                <span>
                    <InnerBoxedDiv>
                        <div className="form-group">
                            <label htmlFor="MyOrders">Your Order</label>
                            <SelectBox className="form-control" id="order" required defaultValue={-1} value={order.name} onChange={changeOrder} name="index" >
                                <DefaultOption disabled value={-1}>Select your option</DefaultOption>
                                {elements.map((order, i) => { return <option key={i} value={i}>{order.name}</option> })};
	                    </SelectBox>
                            <br />
                            <div>
                                <button disabled={!order.index} className="btn btn-dark">Show Delivery Location</button>
                            </div>
                            <br />
                            <div>
                                <button disabled={!order.index} onClick={submit} className="btn btn-success">Accept Delivery</button>
                                <button disabled={!order.index} onClick={cancel} className="btn btn-danger float-right">Cancel Delivery</button>
                            </div>
                        </div>
                    </InnerBoxedDiv>
                </span>
            )
        }
    }

    return (
        <OuterBoxedDiv>
            <form>
                <h4 className='text-center'>My Orders</h4>
                <label htmlFor="Restaurants">Restaurants</label>
                <div>
                    <SelectDiv>
                        <SelectBox className="form-control" id="restaurantsList" required defaultValue={-1} value={restaurant.name} onChange={changeRestaurant} name="index" >
                            <DefaultOption disabled value={-1}>Select your option</DefaultOption>
                            {props.restaurants.map((restaurant, i) => { return <option key={i} value={i}>{restaurant.name}</option> })};
	                    </SelectBox>
                    </SelectDiv>
                </div>
                {restaurant.index ?
                    <div>
                        {showUserPage()}
                        <Map location={props.location} 
                        elements={
                            elements.filter(delivery => delivery.elementAttributes.status === "DELIVERY_ON_THE_WAY").map(element => { return { 'name': element.name, 'location': element.location, 'color': 'green' } })
                            .concat(props.restaurants.map(element => { return { 'name': element.name, 'location': element.location, 'color': 'red' } }))}
                         />
                    </div>
                    : <div />
                }
            </form>
        </OuterBoxedDiv>
    )
};

export default MyOrders;