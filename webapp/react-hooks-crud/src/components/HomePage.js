import React from 'react';
import Logo from '../res/images/logo.jpg';

function HomePage() {
    return (
        <div>
            <h1 style={{ margin: 'auto', textAlign: 'center', color: '#052342' }}>Welcome To TrackEat!</h1>
            <img src={Logo} alt={'Logo'} style={{ display: 'block', margin: 'auto' }} />
        </div>
    );
}

export default HomePage;