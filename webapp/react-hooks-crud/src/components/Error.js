import React from "react";
import { useHistory } from 'react-router-dom';
import { Display, ItemDisplay } from '../styles/Style';

function Error() {
    const history = useHistory();

    return (
        <Display>
            <ItemDisplay>
                <h4>Error</h4>
            </ItemDisplay>
            <ItemDisplay>
                <button className="btn btn-dark" onClick={history.pop}>Back</button>
            </ItemDisplay>
        </Display>
    )
}

export default Error;