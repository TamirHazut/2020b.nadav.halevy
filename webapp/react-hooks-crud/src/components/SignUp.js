import React, { useState } from 'react';
import userDataService from '../services/userService';
import { OuterBoxedDiv, SelectBox, DefaultOption, Button, Display, ItemDisplay } from '../styles/Style';

const initialUserState = {
  email: '',
  role: '',
  username: '',
  avatar: ''
};

function AddUser() {
  const [user, setUser] = useState(initialUserState);
  const [submitted, setSubmitted] = useState(false);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const createUser = async () => {
    userDataService.create(user).then(response => {
      setSubmitted(true);
    }).catch(e => { console.log(e); });
  };

  const newUser = () => {
    setUser(initialUserState);
    setSubmitted(false);
  };

  const preventDefault = (event) => {
    event.preventDefault();
  }

  return (
    submitted ?
      <Display>
        <ItemDisplay>
          <h4> You have successfully submitted!</h4>
        </ItemDisplay>
        <ItemDisplay>
          <Button className='btn btn-success' onClick={newUser}>Register new user</Button>
        </ItemDisplay>
      </Display>
      :
      <OuterBoxedDiv>
        <form onSubmit={preventDefault}>
          <h4  className='text-center'>Sign up</h4>
          <div className='form-group'>
            <label htmlFor='email'>Email</label>
            <input type='text' className='form-control' id='email' required value={user.email} onChange={handleInputChange} name='email' />
          </div>
          <div className='form-group'>
            <label htmlFor='role'>Role</label>
            <SelectBox className='form-control' id='role' required value={user.role} onChange={handleInputChange} name='role' >
              <DefaultOption value=''>Pick a role</DefaultOption>
              <option value='PLAYER'>Player</option>
              <option value='MANAGER'>Manager</option>
              <option value='ADMIN'>Admin</option>
            </SelectBox>
          </div>
          <div className='form-group'>
            <label htmlFor='username'>Username</label>
            <input type='text' className='form-control' id='username' required value={user.username} onChange={handleInputChange} name='username' />
          </div>
          <div className='form-group'>
            <label htmlFor='avatar'>Avatar</label>
            <input type='text' className='form-control' id='avatar' required value={user.avatar} onChange={handleInputChange} name='avatar' />
          </div>
          <Button disabled={!user.email || !user.role || !user.username || !user.avatar} onClick={createUser} className='btn btn-success'>Submit</Button>
        </form>
      </OuterBoxedDiv>
  );

}
export default AddUser;