import React, { useState, useEffect } from 'react';
import userService from '../services/userService';
import { OuterBoxedDiv, Button } from '../styles/Style';

const profile = {
    username: null,
    avatar: null
}

function UserProfile(props) {
    const [userProfile, setUserProfile] = useState(profile);
    const [update, setUpdate] = useState(false);

    useEffect(() => {
        if (update) {
            userService.put(props.user.userId.domain, props.user.userId.email, props.user);
            setUpdate(false);
        }
    }, [update])

    const save = async () => {
        let changed = false;
        if (userProfile.username && userProfile.username !== props.user.username) {
            props.setUser({ ...props.user, username: userProfile.username })
            changed = true;
        }
        if (userProfile.avatar && userProfile.avatar !== props.user.avatar) {
            props.setUser({ ...props.user, avatar: userProfile.avatar })
            changed = true;
        }
        if (changed) {
            setUpdate(true);
        }
    };

    const handleInputChange = event => {
        const { name, value } = event.target;
        setUserProfile({ ...userProfile, [name]: value });
    };
    return (
        <OuterBoxedDiv>
            <h4>Details</h4>
            <div>
                <div className="form-group">
                    <label htmlFor="Username">Username</label>
                    <input type="text" className="form-control" id="username" name="username" onChange={handleInputChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="Avatar">Avatar</label>
                    <input type="text" className="form-control" id="avatar" name="avatar" onChange={handleInputChange} />
                </div>
            </div>
            <Button onClick={save} className="btn btn-success">Save</Button>
        </OuterBoxedDiv>
    );
}

export default UserProfile;