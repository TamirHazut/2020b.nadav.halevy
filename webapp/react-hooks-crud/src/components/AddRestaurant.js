import React, { useState, useEffect } from "react";
import Map from "./Map";
import elementService from "../services/elementService";
import { OuterBoxedDiv, Button, ItemDisplay, Display } from "../styles/Style";
import { useCallback } from "react";

const initialRestaurantState = {
  elementId: null,
  type: "restaurant",
  name: '',
  active: true,
  createdTimestamp: null,
  createdBy: {
    userId: {
      domain: '',
      email: ''
    }
  },
  location: {
    lat: null,
    lng: null
  },
  elementAttributes: {
    Tel: ''
  }
}

function AddRestaurant(props) {
  const [restaurant, setRestaurant] = useState(initialRestaurantState);
  const [subscribed, setSubscribed] = useState(false);

  useEffect(() => {
    if (props.user) {
      restaurant.manager = props.user.userId;
    }
  }, [props.user, restaurant.manager]);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setRestaurant({ ...restaurant, [name]: value });
  };

  const submit = async () => {
    elementService.createNewRestaurant(props.user?.userId.domain, props.user?.userId.email, restaurant).then(response => {
      setSubscribed(true);
    }).catch(e => { console.log(e); })
  }

  const newRestaurant = () => {
    setRestaurant(initialRestaurantState);
    setSubscribed(false);
  };

  const preventDefault = (event) => {
    event.preventDefault();
  }

  useEffect(() => {
    console.log(restaurant.location)
  }, [restaurant.location])

  const getLocation = useCallback((event) => {
    setRestaurant({ ...restaurant, location: { lat: event.lat, lng: event.lng }})
  }, [restaurant])

  return (
    subscribed ?
      <Display>
        <ItemDisplay>
          <h4>You have successfully subscribed!</h4 >
        </ItemDisplay>
        <ItemDisplay>
          <Button className='btn btn-success' onClick={newRestaurant}>Subscribe new restaurant</Button>
        </ItemDisplay>
      </Display>
      :
      <OuterBoxedDiv>
        <h4>Add Restaurant to TrackEat</h4>
        <div className="Login-form">
          <label htmlFor='details'>Restaurant Deatils</label>
          <form onSubmit={preventDefault}>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input type="text" className="form-control" id="name" name="name" value={restaurant.name} onChange={handleInputChange} />
            </div>
            <div className="form-group">
              <label htmlFor="phone">Phone number</label>
              <input type="text" className="form-control" id="phone" name="phone" value={restaurant.elementAttributes.phone} onChange={handleInputChange} />
            </div>
          </form>
        </div>
        <Map location={props.location} onClick={getLocation}></Map>
        <Button disabled={!restaurant.name || !restaurant.elementAttributes.phone || !restaurant.location.lat || !restaurant.location.lng} onClick={submit} className="btn btn-success">Subscribe To TrackEat!</Button>
      </OuterBoxedDiv>
  );

};
export default AddRestaurant;
