import http from "../http-common";

const getSpecificElement = (userDomain, userEmail, elementDomain, elementId) => {
	  return http.get(`/elements/${userDomain}/${userEmail}/${elementDomain}/${elementId}`);
	};
	
const getAllElements = (userDomain,userEmail) => {
	  return http.get(`/elements/${userDomain}/${userEmail}`);
	};
	
const getAllElementsByType = (userDomain,userEmail,type) => {
	return http.get(`/elements/${userDomain}/${userEmail}/search/byType/${type}`)
};
const getAllElementsByName = (userDomain,userEmail,name) => {
	return http.get(`/elements/${userDomain}/${userEmail}/search/byName/${name}`)
};
const createNewRestaurant = (managerDomain,managerEmail,restaurant) => {
	return http.post(`/elements/${managerDomain}/${managerEmail}`,restaurant)
};

const getChildrenElements = (managerDomain,managerEmail,elementDomain,elementEmail) => {
	return http.get(`/elements/${managerDomain}/${managerEmail}/${elementDomain}/${elementEmail}/children`)
};

export default {
  getAllElements,
  getAllElementsByType,
  getSpecificElement,
  createNewRestaurant,
  getChildrenElements,
  getAllElementsByName
};