import http from "../http-common";

const createNewAction = (data) => {
        return http.post('/actions',data);
}

export default {
    createNewAction
    };