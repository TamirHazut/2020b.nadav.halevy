import http from "../http-common";

const get = (userDomain, userEmail) => {
  var user = http.get(`/users/login/${userDomain}/${userEmail}`);
  return user;
};

const create = data => {
  return http.post("/users", data);
};

const put = (userDomain, userEmail, data) => {
  console.log(data)
  http.put(`/users/${userDomain}/${userEmail}`, data);
};

const changeManagerToPlayer = (userId) => {
  var userData = {
    userId: {
      domain: userId.domain,
      email: userId.email
    },
    role: "PLAYER"
  }
  console.log(`/users/${userId.domain}/${userId.email}`);
  return http.put(`/users/${userId.domain}/${userId.email}`, userData)
}
const changePlayerToManager = (userId) => {
  var userData = {
    userId: {
      domain: userId.domain,
      email: userId.email
    },
    role: "MANAGER"
  };
  console.log(`/users/${userId.domain}/${userId.email}`);
  return http.put(`/users/${userId.domain}/${userId.email}`, userData);
}

export default {
  get,
  create,
  put,
  changeManagerToPlayer,
  changePlayerToManager
};