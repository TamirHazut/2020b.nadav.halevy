import styled from "styled-components";

export const BaseNavbar = styled.div`
    display: flex;
    align-items: center;
    height: 50px;
    width: 100vw;
    background-color: blueviolet;
`;

export const MapWrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    height: 50vh;
    width: 100%;
`;

export const Button = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-block-start: 20px;
    font-size: 16px;
    color: white;
    border: 2px solid white;
    border-radius: 15px;
    padding: 15px 20px 15px 20px;
    height: 40px;
    width: ${props => props.width};
`;

export const OuterBoxedDiv = styled.div`
    margin: 50px auto;
    border: 1px solid #9C9C9C;
    width: 500px;
    padding: 20px;
    background: #EAEAEA;
    color: #25598f;
`;

export const InnerBoxedDiv = styled.div`
    margin: 20px auto;
    border: 1px solid #9C9C9C;
    max-width: 500px;
    padding: 20px;
    background: #EAEAEA;
    color: #25598f;
`;

export const FlexDiv = styled.div`
    display: flex;
`;

export const InnerFlexDiv = styled.div`
    margin-top: 10px;
    padding: 5px;
`;

export const SelectDiv = styled.div`
    display: inline-block;
`;

export const SelectBox = styled.select`
    width: 200; 
`;

export const DefaultOption = styled.option`
    display: none;
`;

export const ItemDisplay = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin: auto;
    width: 70%;
`;

export const Display = styled.div`
    margin: 50px auto;
    color: #25598f;
`;

export const StyledUserName = styled.b`
    color: #fff;
`;
